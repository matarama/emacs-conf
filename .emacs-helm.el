
;;_________________________________________________________Completion framework
;; http://tuhdo.github.io/helm-intro.html
(use-package helm
  :delight
  :ensure t
  :defer t
  :init
  (defun my/helm-buffer-size-increase ()
    "Increase helm buffer size by 10"
    (interactive)
    (setq helm-autoresize-min-height (+ helm-autoresize-min-height 10)))

  (defun my/helm-buffer-size-decrease ()
    "Decrease helm buffer size by 10"
    (interactive)
    (setq helm-autoresize-min-height (- helm-autoresize-min-height 10)))

  (defun my/helm-buffer-size-reset ()
    "Resets helm buffer size to 30%"
    (interactive)
    (setq helm-autoresize-min-height 30))

  (setq helm-use-frame-when-more-than-two-windows nil)
  (add-hook 'c-mode-common-hook (lambda () (helm-mode 1)))
  (helm-mode 1)
  :bind (("M-x" . helm-M-x)
         ("M-y" . helm-show-kill-ring)
         ("C-x C-f" . helm-find-files)
         ("C-x b" . helm-mini)
         ("C-x C-b" . helm-buffers-list)
         ("C-h SPC" . helm-all-mark-rings)
         ("C-c h m" . helm-man-woman)
         ("C-c h a" . helm-apropos)
         ("C-c h i" . helm-semantic-or-imenu)
         ("C-c h /" . helm-find)
         ("C-c h l" . helm-locate)
         ("C-c h <tab>" . helm-lisp-completion-at-point)
         ("C-c h b" . helm-resume)
         ("C-c h r" . helm-regexp)
         ("C-c h t" . helm-top)
         ("C-c h M-." . helm-eval-expression-with-eldoc)
         ("C-c h C-," . helm-calcul-expression)
         :map helm-map
         ;; adjust helm buffer size
         ("C-+" . my/helm-buffer-size-increase)
         ("C--" . my/helm-buffer-size-decrease)
         ("C-." . my/helm-buffer-size-reset)
         ;; rebind tab to run persistent action
         ("<tab>" . helm-execute-persistent-action)
         ;; make TAB work in terminal
         ("C-i" . helm-execute-persistent-action)
         ;; TODO make this work within helm-minibuffer
         ;; ("C-h" . describe-foo-at-point)
         ;; list actions using C-z)
         ("C-z" . helm-select-action)
         ("M-n" . helm-next-source)
         ("M-p" . helm-previous-source)
         :map minibuffer-local-map
         ("C-c C-l" . helm-mini-buffer-history))
  :config
  (require 'helm-config)

  ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
  ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
  ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c"))

  ;; open helm buffer inside current window, not occupy whole other window
  (setq helm-split-window-in-side-p t)

  ;; move to end or beginning of source when reaching top or bottom of source.
  (setq helm-move-to-line-cycle-in-source t)

  ;; search for library in `require' and `declare-function' sexp.
  (setq helm-ff-search-library-in-sexp t)

  ;; scroll 8 lines other window using M-<next>/M-<prior>
  (setq helm-scroll-amount 8)
  (setq helm-ff-file-name-history-use-recentf t)
  (setq helm-echo-input-in-header-line t)
  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 30)
  (helm-autoresize-mode 1)

  ;; Use ack-grep instead of grep
  (when (executable-find "ack-grep")
    (setq helm-grep-default-command
          "ack-grep -Hn --no-group --no-color %e %p %f"
          helm-grep-default-recurse-command
          "ack-grep -H --no-group --no-color %e %p %f"))

  ;; optional fuzzy matching for helm-M-x
  (setq helm-M-x-fuzzy-match t)
  (setq helm-semantic-fuzzy-match t)
  (setq helm-imenu-fuzzy-match t)
  (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)
  (setq helm-apropos-fuzzy-match t)
  (setq helm-buffers-fuzzy-matching t)
  (setq helm-recentf-fuzzy-match t)

  ;; make eshell use helm for auto-complete and command history
  ;; https://github.com/emacs-helm/helm/wiki/Eshell
  (require 'helm-eshell)
  (add-hook 'eshell-mode-hook
            (lambda ()
              (eshell-cmpl-initialize)
              (define-key eshell-mode-map [remap eshell-pcomplete] 'helm-esh-pcomplete)
              (define-key eshell-mode-map (kbd "C-c C-l") 'helm-eshell-history)))
  (defun pcomplete/sudo ()
    (let ((prec (pcomplete-arg 'last -1)))
      (cond ((string= "sudo" prec)
             (while (pcomplete-here*
                     (funcall pcomplete-command-completion-function)
                     (pcomplete-arg 'last) t))))))

  ;; make shell use helm for command history
  (add-hook 'shell-mode-hook
            #'(lambda ()
                (define-key shell-mode-map (kbd "C-c C-l")  'helm-comint-input-ring))))

(use-package helm-ag
  :after helm
  :ensure helm
  :ensure ag
  :ensure t
  :defer t
  :delight
  :init
  (add-to-list 'load-path "/opt/ripgrep-0.7.1/rg")
  (if (executable-find "rg")
      (progn
        (setq helm-grep-ag-command "rg --color=always --colors 'match:fg:black' --colors 'match:bg:yellow' --smart-case --no-heading --line-number %s %s %s")
        (setq helm-grep-ag-pipe-cmd-switches '("--colors 'match:fg:black'" "--colors 'match:bg:yellow'")))
    (when (executable-find "ag")
      (setq helm-grep-ag-command "ag --line-numbers -S --hidden --color --color-match '31;43' --nogroup %s %s %s")
      (setq helm-grep-ag-pipe-cmd-switches '("--color-match '31;43'")))))

(use-package helm-rg
  :after helm
  :ensure helm
  :ensure projectile
  :ensure t
  :defer t
  :delight
  :init
  (add-to-list 'load-path "/usr/bin/rg")
  ;; No need use this function.
  ;; helm-projectile already defined a function for this "helm-projectile-rg"
  ;; (defun helm-rg-projectile ()
  ;; 	"Call helm-rg from project's root directory."
  ;; 	(interactive)
  ;; 	(when projectile-mode
  ;; 	  (message "project-root=%s" (projectile-project-root))
  ;; 	  (helm-rg "" nil (list (projectile-project-root)))))
  :bind (;;("C-c s" . helm-rg)
		 :map projectile-mode-map
		 ("C-c p s" . helm-projectile-rg)))

(use-package helm-rhythmbox
  :after helm
  :ensure helm
  :ensure t
  :delight
  :defer t)

(use-package helm-company
  :after (helm company)
  :ensure helm
  :ensure company
  :ensure t
  :defer t
  :delight
  :init
  (eval-after-load 'company
    '(progn
       ;; (define-key company-active-map (kbd ",") 'helm-company)
       (define-key company-active-map (kbd "C-*") 'helm-company))))

;;_________________________________________________Advanced version of i-search
;; https://github.com/ShingoFukuyama/helm-swoop
(use-package helm-swoop
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :config
  (setq helm-swoop-split-with-multiple-windows t)
  :bind
  ("M-i" . helm-swoop))

(use-package helm-make
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :delight
  :bind (("C-c p RET" . helm-make-projectile))
  :init
  (setq helm-make-comint t))

(use-package helm-projectile
  :after (helm projectile)
  :ensure helm
  :ensure projectile
  :ensure helm-rg
  :ensure t
  :defer t
  :delight
  :init
  ;; (add-hook 'c-mode-common-hook (lambda () (helm-projectile-on)))
  (add-hook 'projectile-mode-hook (lambda () (helm-projectile-on)))
  :config
  (define-key projectile-mode-map (kbd "C-c p s") 'helm-projectile-rg))

(use-package helm-dash
  :after (helm switch-window)
  :ensure helm
  :ensure switch-window
  :ensure t
  :delight
  :bind (("C-M-," . my/helm-dash-at-point))
  :init
  ;; Make sure to have "~/.docsets" directory
  (if (not (file-exists-p "~/.docsets"))
      (shell-command "mkdir ~/.docsets"))

  ;; Set display backend
  ;; (setq helm-dash-browser-func 'eww)
  (setq helm-dash-browser-func 'browse-url)
  ;; (setq helm-dash-browser-func 'xwidget-webkit-browse-url)
  ;; (setq helm-dash-browser-func 'eaf-open-url)

  ;; More convenient helm-dash-at-point function
  (defun my/helm-dash-at-point (&optional arg)
    "Run helm-dash-at-point command but display results in a selected window"
    (interactive "P")
    (let ((word)
          (caller-window))
      (copy-word arg)
      (setq word (pop kill-ring))
      (setq caller-window (selected-window))
      (when (> (length (window-list)) 1)
        (switch-window)
        ;; (enlarge-window-horizontally)
        (helm-dash word)
        (select-window caller-window))
      (when (< (length (window-list)) 2)
        (helm-dash word))))

  (helm-dash-ensure-docset-installed "Emacs_Lisp")
  (helm-dash-ensure-docset-installed "LaTeX")
  (helm-dash-ensure-docset-installed "Bash")
  (add-hook 'elisp-mode-hook
            (lambda () (helm-dash-activate-docset "Emacs_Lisp")))
  (add-hook 'latex-mode-hook
            (lambda () (helm-dash-activate-docset "LaTeX")))
  (add-hook 'shell-mode-hook
            (lambda () (helm-dash-activate-docset "Bash")))


  (if (and (not (file-exists-p "~/.docsets/Emacs_Lisp.docset"))
		   (file-exists-p "~/.docsets/Emacs Lisp.docset"))
	  (shell-command
	   "ln -s ~/.docsets/Emacs\ Lisp.docset ~/.docsets/Emacs_Lisp.docset")))

(use-package color-moccur
  :ensure t
  :defer t)

(use-package helm-c-moccur
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :bind ("C-c h o" . helm-occur))

;;____________________Describe keybindings for all open modes in current buffer
(use-package helm-descbinds
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :bind ("C-c h C-h" . helm-descbinds))
