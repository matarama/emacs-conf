
(use-package ivy
  :delight
  :ensure t
  :defer t
  :init
  ;; (add-hook 'c-mode-common-hook (lambda () (ivy-mode 1)))
  (ivy-mode 1)
  :bind (
         
         ("C-x b" . ivy-swicth-buffer)
         ("C-x"))
  )

(use-package counsel
  :delight
  :ensure t
  :defer t
  :init
  :bind (("M-x" . counsel-M-x)
         ("M-y" . counsel-yank-pop)
         ("C-x C-f" . counsel-find-file)
         ("C-c h C-h" . counsel-descbinds)
         )
  )

(use-package swiper
  :delight
  :ensure t
  :defer t
  :init
  )
