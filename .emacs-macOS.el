
;;_______________________________________________________________Mac OS settings

;; set environment variables from .profile
;; https://stackoverflow.com/questions/9663396/how-do-i-make-emacs-recognize-bash-environment-variables-for-compilation
;; -----------------------------------------------------------------------------
;; Set PATH
(let ((path (shell-command-to-string ". ~/.profile; echo -n $PATH")))
  (setenv "PATH" path)
  (setq exec-path 
        (append
         (split-string-and-unquote path ":")
         exec-path)))

;; Set all other environment variables
(let* ((tmp (shell-command-to-string ". ~/.profile; printenv"))
       (vars (split-string tmp "\n")))
  (while vars
    (let* ((pair (split-string (pop vars) "="))
           (name (pop pair))
           (value (pop pair)))
      ;; Do not overwrite values that are already set
      (when (not (getenv name))
        (setenv name value))

      ;; Mac OS El Captain fix for variables that are uninheritable
      (when (string= "LIBRARY_PATH" name)
        (setenv (concat "LD_LIBRARY_PATH" value))
        (setenv (concat "DYLD_LIBRARY_PATH" value))
        (setenv (concat "DYLD_FALLBACK_LIBRARY_PATH" value))))))

;; Turn off graphical dialogs on Mac OS
;; https://superuser.com/questions/125569/how-to-fix-emacs-popup-dialogs-on-mac-os-x
;; -----------------------------------------------------------------------------
(defadvice yes-or-no-p (around prevent-dialog activate)
  "Prevent yes-or-no-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))
(defadvice y-or-n-p (around prevent-dialog-yorn activate)
  "Prevent y-or-n-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))

;; Adjust Mac OS command key
;; -----------------------------------------------------------------------------
(setq mac-command-modifier 'meta)
(setq mac-right-option-modifier 'none)
(setq mac-left-option-modifier 'none)

;; (when (require 'company nil 'noerror)
;;   (define-key company-mode-map (kbd "C-'") 'company-complete-common))

;; (when (require 'helm-company nil 'noerror)
;;   (define-key company-mode-map (kbd "C-'") 'helm-company))

;; org-mode custom reference to pages of pdf documents
;; https://emacs.stackexchange.com/questions/30344/how-to-link-and-open-a-pdf-file-to-a-specific-page-skim-adobe
;; (org-add-link-type "pdf" 'org-pdf-open nil)
;; (defun org-pdf-open (link)
;; "Where page number is 105, the link should look like:
;; [[pdf:/path/to/file.pdf#105][My description.]]"
;;   (let* ((path+line (split-string link "#"))
;;          (pdf-file (car path+line))
;;          (base-filename (file-name-nondirectory pdf-file))
;;          (page (car (cdr path+line)))
;;          (script
;;           (format
;;             "tell document \"%s\" of application \"Skim\" to go to page %s"
;;             base-filename page))
;;          (skim-program "/Applications/Skim.app/Contents/MacOS/Skim"))
;;     (start-process "skim-process" nil "open" "-a" skim-program pdf-file)
;;     (start-process "goto-page" nil "osascript" "-e" script)))
