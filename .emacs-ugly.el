
;;_________________________________________________________________________UGLY

;;__________________use of follow-mode without destroying current window layout

(define-minor-mode concentratation
  "Use zoom-window package and follow-mode together to scroll through single
file using at max 3 column layout. Upon exiting the mode, previous window
configuration is restored."
  :init-value nil
  ;; :lighter " zf"
  ;; :keymap zoom-follow-mode-map
  ;; '(([ESC] . zoom-follow-exit))
  ;; :global t
  (require 'zoom-window)
  (concentration-setup))

(defvar concentration-window-layout-hook 'my/coding/max-three-column-layout
  "A hook to function to create window layout. Defaults to
my/coding/max-three-column-layout.")

(defvar concentration-pre-selected-window nil
  "Selected window before running zoom window")

(defun concentration-setup ()
  "Uses zoom-window package to zoom current window and open at most 3 columns
of the current buffer. Upon exiting zoom-window mode, follow-mode for the
current buffer will be deactivated."
  (require 'zoom-window)
  (my/split-window-right)
  (zoom-window-zoom)
  (run-hooks 'concentration-window-layout-hook)
  (follow-mode)
  (setq concentration-pre-selected-window (selected-window)))

(defun lose-concentration ()
  "Exits zoom-window-zoom, follow-mode, and restores window layout."
  (interactive)
  (setq follow-mode nil)
  (zoom-window-zoom)
  (delete-window)
  (select-window concentration-pre-selected-window)
  (setq concentration nil))

;;__________________create compile & run layout/perspective for a given project
(defun my/coding/compile-and-run-layout (&optional eshell-line-count)
  "Create following layout on the current project (when in projectile mode)
|-------------------------------------|
|                  |                  |
|   valg.out       |  *compilation*   |
|                  |                  |
|                  |                  |
|-------------------------------------|
|           *eshell* (cursor)         |
|-------------------------------------|

Height of eshell window is 7 lines."
  (interactive)
  (unless eshell-line-count (setq eshell-line-count 7))
  (when projectile-mode
    (delete-other-windows)
    (split-window-vertically (- (window-total-height) eshell-line-count))
    (select-window (next-window) t)
    (command-execute 'eshell)
    (select-window (previous-window) t)
    (split-window-horizontally)
    (find-file (concat (projectile-project-root) "/valg.out"))
    (select-window (next-window) t)
    (switch-to-buffer (get-buffer "*compilation*"))
    (select-window (next-window) t)))

(defun my/coding/max-three-column-layout ()
  "Create at max 3 column layout where each window will be at least 80
character wide.

If there isn't enough space for 3 window, try spliting into 2 columns
in which left window will be at least 80 character wide and the right window
50 character wide.

If there isn't enough enough space for 2 windows, then don't split.

Cursor will be placed in left-most window."
  (interactive)
  (delete-other-windows)
  (message "total-window-width=%d" (window-total-width))
  (if (> (window-total-width) 245 )
      (progn (split-window-horizontally (/ (+ (window-total-width) 2) 3))
             (select-window (next-window) t)
             (split-window-horizontally (/ (+ (window-total-width) 1) 2))
             (select-window (previous-window) t))
    (if (> (window-total-width) 162)
        (split-window-horizontally (/ (+ (window-total-width) 1) 2))
      (if (> (window-total-width) 132) (split-window-horizontally 80)))))

(defun my/coding/compiled-lang-layout ()
  "There are two frames in c++-layout;
Coding frame         -> see coding/max-three-column-layout ()
Copmile & Run frame  -> see coding/compile-and-run-layout (eshell-line-count)"
  (interactive)
  (my/coding/max-three-column-layout)
  (make-frame '((NAME . "compile-and-run")))
  (select-frame (next-frame) t)
  (my/coding/compile-and-run-layout)
  (select-frame (previous-frame) t))

;;________________________________________________org-mode spreadsheet commands
(defun my/thousand-seperator (text)
  "input: 1234567 Output 1.234.567"
  ;; (interactive)
  (setq len (length text))
  (if (<= len 3)
      (setq ret text)
    (progn
      (setq ret (substring text (- len 3) len))
      (setq len (- len 3))
      (while (> len 3)
        (setq ret (concat (substring text (- len 3) len) "." ret))
        (setq len (- len 3))
        )
      (if (> len 0)
          (setq ret (concat (substring text 0 len) "." ret))))))
