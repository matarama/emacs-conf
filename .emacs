;; Add Melpa to repository list
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa"	.	"http://melpa.org/packages/")
   t)
  (package-initialize))

;; debug on error
;; (setq debug-on-error t)
;;___________________________________________________________ASTHETICS/BEHAVIOR
;; don't show tool-bar, scroll-bar, and menu-bar
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)

;; set font size
(set-face-attribute 'default nil :height 110)
(set-face-attribute 'default nil :font "Ubuntu Mono")

;; unicode characters
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; System locale to use for formatting time values.
(setq system-time-locale "C")

;: automatically refresh all files when updated
(global-auto-revert-mode t)

;; Do not highlight marked region
(setq transient-mark-mode nil)

;; Make startup screen scratch buffer
(setq inhibit-startup-screen t)

;; highlight matching parenthesis
(show-paren-mode 1)

;; lines wrapped around beautifuly
(global-visual-line-mode 1)

;; highlight the line cursor is on
(global-hl-line-mode 1)

;; How to make dired use the same buffer for viewing directory?
;; http://ergoemacs.org/emacs/emacs_dired_tips.html
(require 'dired )
(define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file) ; was dired-advertised-find-file
(define-key
  dired-mode-map (kbd "^")
  (lambda () (interactive) (find-alternate-file "..")))  ;; was dired-up-directory

;; eshell shortcut
(global-set-key "\C-xt" 'eshell)

;; make compilation buffer scroll down
(setq compilation-scroll-output t)
(setq compilation-scroll-output 'first-error)

;; if a buffer is opened in other frame, reuse it
(setq display-buffer-reuse-frames t)

;; http://ergoemacs.org/emacs/emacs_alias.html
(defalias 'yes-or-no-p 'y-or-n-p) ; y or n is enough
(defalias 'list-buffers 'ibuffer) ; always use ibuffer

;; Remove trailing whitespace
;; https://www.emacswiki.org/emacs/DeletingWhitespace#toc3
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; https://sriramkswamy.github.io/dotemacs/
;; Move correctly over camelCased words
(subword-mode 1)
(global-set-key "\M-f" 'subword-right)
(global-set-key "\M-b" 'subword-left)

;; Emacs thinks a sentence is a full-stop followed by 2 spaces.
;; Let’s make it full-stop and 1 space.
(setq sentence-end-double-space nil)
;; make gdb multi-windowed
(setq gdb-many-windows t
      gdb-show-main t)
;; ??
(setq ediff-window-setup-function 'ediff-setup-windows-plain
      ediff-split-window-function 'split-window-horizontally)
;; Narrow to region
(put 'narrow-to-region 'disabled nil)

;; Install use-package if it is not installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(require 'bind-key)

;; Use unicode characters in emacs-lisp and org mode buffers
(add-hook 'emacs-lisp-mode-hook 'prettify-symbols-mode)
(add-hook 'org-mode-hook 'prettify-symbols-mode)


;;___________________________________________________Restart emacs within emacs
(use-package restart-emacs
  :ensure t
  :bind* (("C-x M-c" . restart-emacs)))

;;____________________________________________________To define sticky bindings
(use-package hydra
  :ensure t
  :defer t
  :delight
  :init
  (defun my/reverse-undo ()
    "Reverses undo (does nothing)"
    (message "Undo reversed."))

  (defhydra hydra-undo-redo (global-map "C-x")
    "undo/reverse-undo"
    ("u" undo)
    ("r" (my/reverse-undo))))

;;______________________________________________Clean up mode line with delight
(use-package delight
  :ensure t)

;; enable some minor modes
(use-package emacs
  :delight
  (visual-line-mode)
  (eldoc-mode))

;;___________________________________________________________Choose color theme
(use-package color-theme-sanityinc-tomorrow
  :ensure t
  :defer t
  :init
  ;; (load-theme 'sanityinc-tomorrow-blue t)
  ;; (load-theme 'sanityinc-tomorrow-bright t)
  ;; (load-theme 'sanityinc-tomorrow-day t)
  ;; (load-theme 'sanityinc-tomorrow-eighties t)
  ;; (load-theme 'sanityinc-tomorrow-night t)
  :config)

(use-package ample-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'ample t)
  :config)

(use-package base16-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'base16-classic-dark t)
  ;; (load-theme 'base16-solarized-dark t)
  :config)

(use-package color-theme-solarized
  :ensure t
  :defer t
  :init
  ;; (load-theme 'solarized t)
  :config)

(use-package borland-blue-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'borland-blue t)
  :config)

(use-package green-screen-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'green-screen t)
  :config
  (set-face-attribute 'default nil :height 120)
  (set-face-attribute 'default nil :font "Ubuntu Mono"))

(use-package nimbus-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'nimbus t)
  :config)

(use-package darktooth-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'darktooth t)
  :config)

(use-package kaolin-themes
  :ensure t
  :defer t
  :init
  ;; (load-theme 'kaolin-dark t)
  ;; (load-theme 'kaolin-aurora t)
  ;; (load-theme 'kaolin-bubblegum t)
  ;; (load-theme 'kaolin-light t)
  ;; (load-theme 'kaolin-eclipse t)
  ;; (load-theme 'kaolin-ocean t)
  ;; (load-theme 'kaolin-galaxy t)
  ;; (load-theme 'kaolin-temple t)
  ;; (load-theme 'kaolin-valley-dark t)
  :config)

(use-package gruber-darker-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'gruber-darker t)
  :config)

(use-package spacemacs-theme
 :ensure t
  :defer t
  :init
  ;; (load-theme 'spacemacs-dark t)
  ;; (load-theme 'spacemacs-light t)
  :config)

(use-package eclipse-theme
  :ensure t
  :defer
  :init
  ;; (load-theme 'eclipse t)
  :config)

(use-package grayscale-theme
  :ensure t
  :defer
  :init
  ;; (load-theme 'grayscale t)
  :config)

(use-package minimal-theme
  :ensure t
  :defer
  :init
  ;; (load-theme 'minimal-light t)
  ;; (load-theme 'minimal-black t)
  :config)

(use-package material-theme
  :ensure t
  :defer
  :init
  ;; (load-theme 'material-light t)
  (load-theme 'material t)
  :config)

(use-package immaterial-theme
  :ensure t
  :defer
  :init
  ;; (load-theme 'immaterial t)
  :config)

(use-package doom-themes
  :ensure t
  :defer
  :init
  ;; (load-theme 'doom-one t)
  ;; (load-theme 'doom-one-light t)
  ;; (load-theme 'doom-vibrant t)
  ;; (load-theme 'doom-citylights t)
  ;; (load-theme 'doom-dracula t)
  ;; (load-theme 'doom-losvkem t)
  ;; (load-theme 'doom-molokai t)
  ;; (load-theme 'doom-nord t)
  ;; (load-theme 'doom-nord-light t)
  ;; (load-theme 'doom-nova t)
  ;; (load-theme 'doom-peacock t)
  ;; (load-theme 'doom-solarized-light t)
  ;; (load-theme 'doom-spacegrey t)
  ;; (load-theme 'doom-tomorrow-night t)
  :config)

;;____________________________________________________Move to beginning of line
;; http://emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/
(defun my/smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; remap C-a to `smarter-move-beginning-of-line'
;; (global-set-key [remap move-beginning-of-line]
;;                 'my/smarter-move-beginning-of-line)
(global-set-key "\C-a" 'my/smarter-move-beginning-of-line)

;;________________________________________________describe this point lisp only
;; https://www.emacswiki.org/emacs/DescribeThingAtPoint
(defun my/describe-foo-at-point ()
  "Show the documentation of the Elisp function and variable near point.
	This checks in turn:
	-- for a function name where point is
	-- for a variable name where point is
	-- for a surrounding function call
	"
  (interactive)
  (let (sym)
	;; sigh, function-at-point is too clever.  we want only the first half.
	(cond ((setq sym (ignore-errors
                       (with-syntax-table emacs-lisp-mode-syntax-table
                         (save-excursion
                           (or (not (zerop (skip-syntax-backward "_w")))
                               (eq (char-syntax (char-after (point))) ?w)
                               (eq (char-syntax (char-after (point))) ?_)
                               (forward-sexp -1))
                           (skip-chars-forward "`'")
        	               (let ((obj (read (current-buffer))))
                             (and (symbolp obj) (fboundp obj) obj))))))
           (describe-function sym))
          ((setq sym (variable-at-point)) (describe-variable sym))
          ;; now let it operate fully -- i.e. also check the
          ;; surrounding sexp for a function call.
          ((setq sym (function-at-point)) (describe-function sym)))))

(global-set-key (kbd "<f12>") 'my/describe-foo-at-point)

(defun my/find-foo-at-point ()
  "Jump to Elisp function and variable near point.
	This checks in turn:
	-- for a function name where point is
	-- for a variable name where point is
	-- for a surrounding function call
	"
  (interactive)
  (let (sym)
	;; sigh, function-at-point is too clever.  we want only the first half.
	(cond ((setq sym (ignore-errors
                       (with-syntax-table emacs-lisp-mode-syntax-table
                         (save-excursion
                           (or (not (zerop (skip-syntax-backward "_w")))
                               (eq (char-syntax (char-after (point))) ?w)
                               (eq (char-syntax (char-after (point))) ?_)
                               (forward-sexp -1))
                           (skip-chars-forward "`'")
        	               (let ((obj (read (current-buffer))))
                             (and (symbolp obj) (fboundp obj) obj))))))
           (find-function sym))
          ((setq sym (variable-at-point)) (find-variable sym))
          ;; now let it operate fully -- i.e. also check the
          ;; surrounding sexp for a function call.
          ((setq sym (function-at-point)) (find-function sym)))))

(defun my/find-foo-at-point-wrapper()
  (interactive)
  (let ((caller-window))
    (setq caller-window (selected-window))
    (when (> (length (window-list)) 1)
      (switch-window)
      (my/find-foo-at-point)
      ;; (select-window caller-window)
      )
    (when (< (length (window-list)) 2)
      (my/find-foo-at-point))))

(define-key emacs-lisp-mode-map (kbd "M-.") 'my/find-foo-at-point-wrapper)

;;_____________________make backup to a designated dir, mirroring the full path
;; http://ergoemacs.org/emacs/emacs_set_backup_into_a_directory.html
(defun my/backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* ((backupRootDir "~/.emacs.d/emacs-backup/")
         ;; remove Windows driver letter in path, for example, “C:”
         (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath ))
         (backupFilePath (replace-regexp-in-string
                          "//" "/" (concat backupRootDir filePath "~"))))
    (make-directory (file-name-directory backupFilePath)
                    (file-name-directory backupFilePath))
    backupFilePath)
  )

(setq make-backup-file-name-function 'my/backup-file-name)

;;__________________________________________renames current buffer and its file
;; source: http://steve.yegge.googlepages.com/my-dot-emacs-file
(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

;;_____________________________________when window is split, move to new window
(defun my/split-window-right ()
  "Splits window vertically and moves the focus to new window."
  (interactive)
  (split-window-right)
  (other-window 1))

(defun my/split-window-below ()
  "Splits window vertically and moves the focus to new window."
  (interactive)
  (split-window-below)
  (other-window 1))

(global-set-key (kbd "C-x 3") 'my/split-window-right)
(global-set-key (kbd "C-x 2") 'my/split-window-below)

;;_____________________Faster navigation than next-line slower than scroll-down

(defun my/next-line ()
  "Call next-line function multiple times."
  (interactive)
  (let ((c 0))
	(while (< c 5)
	  (setq c (+ 1 c))
	  (next-line))))

(defun my/previous-line ()
  "Call previous-line function multiple times."
  (interactive)
  (let ((c 0))
	(while (< c 5)
	  (setq c (+ 1 c))
	  (previous-line))))


;; (global-set-key (kbd "M-n") 'my/next-line)
;; (global-set-key (kbd "M-p") 'my/previous-line)

;;_____________________________________Scroll up/down without moving the cursor
(global-set-key (kbd "M-n") 'scroll-up-line)
(global-set-key (kbd "M-p") 'scroll-down-line)

;;___________________________(move selected region / yank line) to other window
;; https://emacs.stackexchange.com/questions/3743/how-to-move-region-to-other-window
(defun move-region-to-other-window (start end)
  "Move selected text to other window"
  (interactive "r")
  (if (use-region-p)
      (let ((count (count-words-region start end)))
        (save-excursion
          (kill-region start end)
          (other-window 1)
          (yank)
          (newline))
        (other-window -1)
        (message "Moved %s words" count))
    (message "No region selected")))

(defun kill-line-to-other-window ()
  "Kill line and yank it to other window"
  (interactive)
  (kill-line)
  (other-window 1)
  (yank)
  (newline)
  (other-window -1))

(global-set-key [(control 252)] 'kill-line-to-other-window)

;;__Most Eshell commands are available only after Eshell has been started once.
;; https://github.com/emacs-helm/helm/wiki/Find-Files
(add-hook 'emacs-startup-hook
          (lambda ()
            (let ((default-directory (getenv "HOME")))
              (command-execute 'eshell)
              ;; Commands that need a pager/curses
              (add-to-list 'eshell-visual-commands "top")
              (add-to-list 'eshell-visual-commands "htop")
              (add-to-list 'eshell-visual-options '("git" "--help"))
              (add-to-list 'eshell-visual-subcommands
                           '("git" "log" "diff" "show" ))
              (bury-buffer))))



;;__________________which-key: Useful for showing major/minor mode key-mappings
;; https://www.youtube.com/watch?v=I28jFkpN5Zk
(use-package which-key
  :delight
  :ensure t
  :defer t
  :init
  (which-key-mode)
  :config
  ;; (which-key-setup-minibuffer)
  (which-key-setup-side-window-right)
  ;; (setq which-key-separator " :")
  ;; (setq which-key-separator " → " )
  (setq which-key-idle-delay 1.0)
  (setq which-key-idle-secondary-delay 0)
  (setq which-key-show-prefix 'top)
  (setq which-key-add-column-padding 0))

;;________________________________Never lose cursor when switching buffers etc.
(use-package beacon
  :delight
  :ensure t
  :config
  (beacon-mode 1))

;;_____________________________________________________________Better mode line
(use-package smart-mode-line
  :delight
  :ensure t
  :init
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'respectful)
  (sml/setup)
  ;; (use-package smart-mode-line-powerline-theme
  ;; :ensure t
  ;; :config
  ;; (setq sml/theme 'powerline))
  )

;;_________________________________________________________________________TABS

;; Set tab size
(setq-default c-basic-offset 4
              tab-width 4
              ;; indent-tabs-mode t
              indent-tabs-mode nil)
;; (add-hook 'emacs-lisp-mode-hook (lambda () (setq indent-tabs-mode nil)))

;; Use both TABs and SPACEs
;; https://www.emacswiki.org/emacs/SmartTabs
;; ----------------------------------------------------------------------------
;; (use-package smart-tabs-mode
;;   :delight smart-tab-mode
;;   :ensure t
;;   :defer t
;;   :init
;;   (add-hook 'c-mode-common-hook (lambda () (setq indent-tabs-mode t)))
;;   (smart-tabs-insinuate 'c 'c++ 'java))


;;_______________________________________________________________BASE_FUNCTIONS

;; Copy word at point (used inside other functions)
(defun get-point (symbol &optional arg)
  "get the point"
  (funcall symbol arg)
  (point)
  )

(defun copy-thing (begin-of-thing end-of-thing &optional arg)
  "copy thing between beg & end into kill ring"
  (save-excursion
	(let ((beg (get-point begin-of-thing 1))
          (end (get-point end-of-thing arg)))
	  (copy-region-as-kill beg end)))
  )

(defun paste-to-mark(&optional arg)
  "Paste things to mark, or to the prompt in shell-mode"
  (let ((pasteMe
         (lambda()
           (if (string= "shell-mode" major-mode)
               (progn (comint-next-prompt 25535) (yank))
             (progn (goto-char (mark)) (yank) )))))
    (if arg
        (if (= arg 1)
            nil
          (funcall pasteMe))
      (funcall pasteMe))
    ))

(defun copy-word (&optional arg)
  "Copy words at point into kill-ring"
  (interactive "P")
  (copy-thing 'backward-word 'forward-word arg)
  ;;(paste-to-mark arg)
  )

;;______________________________________________________________________EDITING

;;___________________Multiple cursors for editing multiple lines simultaneously
;; https://github.com/magnars/multiple-cursors.el
(use-package multiple-cursors
  :delight
  :ensure t
  :defer t
  :bind (("C-C C-C" . mc/edit-lines)
         ("C->" . mc/mark-next-like-this)
         ("C-<" . mc/mark-previous-like-this)
         ("C-c C-<" . mc/mark-all-like-this)))

;;_________________________________from: Emacs Cookbook - put commas in numbers
;; (defun group-number (num &optional size char)
;;   "Format NUM as string grouped to SIZE with CHAR."
;;   ;; Based on code for `math-group-float' in calc-ext.el
;;   (interactive)
;;   (let* ((size (or size 3))
;;          (char (or char ","))
;;          (str (if (stringp num)
;;                   num
;;                 (number-to-string num)))
;;          ;; omitting any trailing non-digit chars
;;          ;; NOTE: Calc supports BASE up to 36 (26 letters and 10 digits ;)
;;          (pt (or (string-match "[^0-9a-zA-Z]" str) (length str))))
;;     (while (> pt size)
;;       (setq str (concat (substring str 0 (- pt size))
;;                         char
;;                         (substring str (- pt size)))
;;             pt (- pt size)))
;;     str))

;;_____________________________________79 character comment from right function
(defun xah-comment-line ()
  "Print number of words and chars in text selection or line.
In emacs 24, you can use `count-words'."
  (interactive)
  (let (mp p1 p2)
    (if (region-active-p)
        (progn (setq p1 (region-beginning))
               (setq p2 (region-end)))
      (progn
        (back-to-indentation)
        (setq p1 (point))
        (setq p2 (line-end-position))))

    (save-excursion
      (let (charCnt)
        (back-to-indentation)
        (setq mp (point))
        (setq charCnt ( - p2 p1 ))
        (insert-char ?\; 2)
        (setq charCnt (- p2 p1))
        (while (< (current-column) ( - 79 charCnt ))
          (insert-char ?_))))))

;;_______________________79 character comment from right function (c/java ver.)
(defun my/coding/javac-section ()
  "Print number of words and chars in text selection or line.
In emacs 24, you can use `count-words'."
  (interactive)
  (let (mp p1 p2)
    (if (region-active-p)
        (progn (setq p1 (region-beginning))
               (setq p2 (region-end)))
      (progn
        (back-to-indentation)
        (setq p1 (point))
        (setq p2 (line-end-position))))

    (save-excursion
      (let (charCnt)
        (back-to-indentation)
        (setq mp (point))
        (setq charCnt ( - p2 p1 ))
        (insert-char ?\\ 2)
        (setq charCnt (- p2 p1))
        (while (< (current-column) ( - 79 charCnt ))
          (insert-char ?_))))))

;;_____________________________________________Seperator comment for c/c++/java
(defun my/coding/javac-separator ()
  "Insert following c++ command in current buffer (79 character):
// ----------------------------------------------------------------------------

Note that length of the comment will be 79 characters together with
current column. For example>
int a = 4;// ------------------------------------------------------------------
"
  (interactive)
  (let ((amount-to-pad (- 76 (current-column))))
    (when (> amount-to-pad 0)
      (message "current-indentation: %d" (current-indentation))
      (insert "// ")
      (insert-char ?- amount-to-pad))))


;;_________________________________________________________________File manager
(use-package ranger
  :ensure t
  :init
  :config
  :bind (:map ranger-mode-map
              ("n" . ranger-next-file)
              ("p" . ranger-prev-file)
              ("C-f" . ranger-find-file)
              ("C-b" . ranger-up-directory)
              ("C-a" . ranger-goto-top)
              ("C-e" . ranger-goto-bottom)
              ("!" . ranger-pop-shell)
              ("C-d" . ranger-to-dired)))

;;____________________________________________________________Org mode settings
(use-package org
  :ensure company
  :ensure t
  :init
  (setq org-catch-invisible-edits 'show-and-error)
  (setq org-latex-create-formula-image-program 'dvipng)
  (setq org-file-apps
        '((auto-mode . emacs)
          ("\\.x?html?\\'" . "firefox %s")
          ("\\.ods" . "libreoffice \"%s\"")
          ("\\.pdf\\'" . "evince \"%s\"")
          ("\\.pdf::\\([0-9]+\\)\\'" . "evince \"%s\" -p %1")
          ("\\.pdf.xoj" . "xournal %s")))
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((latex . t) (C . t ) (python . t) (java . t) (shell . t)))

  (defun my-org-mode-hook ()
    (add-hook 'completion-at-point-functions
              'pcomplete-completions-at-point nil t)
    (add-to-list 'company-backends 'company-capf)
    ;; (setq company-auto-complete-chars "+")
    ;; (setq company-auto-complete t)
    )

  (add-hook 'org-mode-hook #'my-org-mode-hook)
  :config
  (bind-keys
   :map org-mode-map
   ("\C-a" . my/smarter-move-beginning-of-line)))

(use-package org-babel-eval-in-repl
  :ensure t)

(use-package ob-async
  :ensure t)

(use-package auctex
  :ensure t
  :ensure company-auctex
  :ensure yasnippet
  :defer t
  :init
  (setq company-auto-complete-chars "\\$")
  (setq company-auto-complete t))

(use-package latex-extra
  :ensure t
  :defer t
  :delight)
(use-package latex-math-preview
  :ensure t
  :defer t
  :delight)
(use-package latex-pretty-symbols
  :ensure t
  :defer t
  :delight)
(use-package latex-preview-pane
  :ensure t
  :defer t
  :delight)

(require 'ox-latex)
(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("tik"
                 "\\documentclass{article}
                   \\usepackage[left=0.75in,top=0.75in,right=1in,bottom=1.25in]{geometry}
                   \\usepackage{graphicx}
                   \\usepackage[usenames,dvipsnames]{pstricks}
                   \\usepackage{epsfig}
                   \\usepackage{pst-grad}
                   \\usepackage{pst-plot}
                   \\usepackage[space]{grffile}
                   \\usepackage{etoolbox}
            [NO-DEFAULT-PACKAGES]
            [NO-PACKAGES]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
)

;; (org-babel-do-load-languages 'org-babel-load-languages '((python . t)))
;; (defun add-pcomplete-to-capf ()
;;   (add-hook 'completion-at-point-functions 'pcomplete-completions-at-point nil t))
;; (add-hook 'org-mode-hook #'add-pcomplete-to-capf)

(defun my-org-mode-config ()
  (local-set-key (kbd "C-,") 'company-complete-common))
(add-hook 'org-mode-hook #'my-org-mode-config)

;; Use org-ref to handle citations automatically
;; http://kitchingroup.cheme.cmu.edu/blog/2015/12/22/org-ref-is-on-Melpa/
;; -----------------------------------------------------------------------------
;; (use-package org-ref
;;   :ensure t
;;   :defer t
;;   :init
;;   (setq reftex-default-bibliography '("~/Documents/bibliography/references.bib"))
;;   (setq org-ref-bibliography-notes "~/Documents/bibliography/notes.org"
;; 		org-ref-default-bibliography '("~/Documents/bibliography/references.bib")
;; 		org-ref-pdf-directory "~/Documents/bibliography/bibtex-pdfs/")
;;   :config
;;   )

;; Use ox-clip to copy formatted text from org mode buffer to other applications
;; http://kitchingroup.cheme.cmu.edu/blog/2016/06/16/Copy-formatted-org-mode-text-from-Emacs-to-other-applications/
;; -----------------------------------------------------------------------------
;; (use-package ox-clip
;;   :ensure t
;;   :defer t)

;; (setq scimax-src-block-keymaps
;;       '(("ipython" . ,(let ((map (make-composed-keymap
;;                                   '(,elpy-mode-map ,python-mode-map ,pyvenv-mode-map)
;;                                   org-mode-map)))
;;                         ;; In org-mode I define RET so we f
;;                         (define-key map (kbd "<return>") 'newline)
;;                         (define-key map (kbd "C-c C-c") 'org-ctrl-c-ctrl-c)
;;                         map))
;;         ("python" . ,(let ((map (make-composed-keymap
;;                                  '(,elpy-mode-map ,python-mode-map ,pyvenv-mode-map)
;;                                  org-mode-map)))
;;                        ;; In org-mode I define RET so we f
;;                        (define-key map (kbd "<return>") 'newline)
;;                        (define-key map (kbd "C-c C-c") 'org-ctrl-c-ctrl-c)
;;                        map))
;;         ("emacs-lisp" . ,(let ((map (make-composed-keymap '(,lispy-mode-map
;;                                                             ,emacs-lisp-mode-map
;;                                                             ,outline-minor-mode-map)
;;                                                           org-mode-map)))
;;                            (define-key map (kbd "C-c C-c") 'org-ctrl-c-ctrl-c)
;;                            map))))

;; (defun scimax-add-keymap-to-src-blocks (limit)
;;   "Add keymaps to src-blocks defined in `scimax-src-block-keymaps'."
;;   (let ((case-fold-search t)
;;         lang)
;;     (while (re-search-forward org-babel-src-block-regexp limit t)
;;       (let ((lang (match-string 2))
;;             (beg (match-beginning 0))
;;             (end (match-end 0)))
;;         (if (assoc (org-no-properties lang) scimax-src-block-keymaps)
;;             (progn
;;               (add-text-properties
;;                beg end '(local-map ,(cdr (assoc
;;                                           (org-no-properties lang)
;;                                           scimax-src-block-keymaps))))
;;               (add-text-properties
;;                beg end '(cursor-sensor-functions
;;                          ((lambda (win prev-pos sym)
;;                             ;; This simulates a mouse click and makes a menu change
;; 							;; (org-mouse-down-mouse nil)
;; 							))))))))))

;; (defun scimax-spoof-mode (orig-func &rest args)
;;   "Advice function to spoof commands in org-mode src blocks.
;; It is for commands that depend on the major mode. One example is
;; 'lispy--eval'."
;;   (if (org-in-src-block-p)
;;       (let ((major-mode (intern (format "%s-mode" (first (org-babel-get-src-block-info))))))
;;         (apply orig-func args))
;;     (apply orig-func args)))

;; (define-minor-mode scimax-src-keymap-mode
;;   "Minor mode to add mode keymaps to src-blocks."
;;   :init-value nil
;;   (if scimax-src-keymap-mode
;;       (progn
;;         (add-hook 'org-font-lock-hook #'scimax-add-keymap-to-src-blocks t)
;;         (add-to-list 'font-lock-extra-managed-props 'local-map)
;;         (add-to-list 'font-lock-extra-managed-props 'cursor-sensor-functions)
;;         (advice-add 'lispy--eval :around 'scimax-spoof-mode)
;;         (cursor-sensor-mode +1))
;;     (remove-hook 'org-font-lock-hook #'scimax-add-keymap-to-src-blocks)
;;     (advice-remove 'lispy--eval 'scimax-spoof-mode)
;;     (cursor-sensor-mode -1))
;;   (font-lock-fontify-buffer))

;; (add-hook 'org-mode-hook (lambda ()
;;                            (scimax-src-keymap-mode +1)))

;;____________________________________________________NAVIGATION/FRAMES/WINDOWS

;;_________Use isearch in other window while restoring cursor to current buffer
(defun my/isearch-forward/other-window ()
  (interactive)
  (when (> (length (window-list)) 1)
    (save-selected-window
      (select-window (next-window) t)
      (isearch-forward))))

(defun my/isearch-backward/other-window ()
  (interactive)
  (when (> (length (window-list)) 1)
    (save-selected-window
      (select-window (next-window) t)
      (isearch-backward))))

(global-set-key (kbd "C-c s") 'my/isearch-forward/other-window)
(global-set-key (kbd "C-c r") 'my/isearch-backward/other-window)

;;____________________________________________________Move focus between frames
(defun my/next-frame ()
  "Move focus to 'next frame'."
  (interactive)
  (let ((nf (next-frame)))
    (select-frame-set-input-focus nf))
  )

(defun my/previous-frame ()
  "Move focus to 'previous frame'."
  (interactive)
  (let ((pf (previous-frame)))
    (select-frame-set-input-focus pf))
  )

(global-set-key (kbd "M-<f10>") 'my/next-frame)
(global-set-key (kbd "M-<f9>") 'my/previous-frame)

;;______________________________________________move between windows, taken from
;; https://www.emacswiki.org/emacs/WindMove
;; -----------------------------------------------------------------------------
(setq windmove-wrap-around t)
(global-set-key (kbd "C-c k") 'windmove-left)
(global-set-key (kbd "C-c ş") 'windmove-right)
(global-set-key (kbd "C-c o") 'windmove-up)
(global-set-key (kbd "C-c l") 'windmove-down)
(global-set-key [f9] 'previous-buffer)
(global-set-key [f10] 'next-buffer)
(global-set-key (kbd "M-P") 'previous-buffer)
(global-set-key (kbd "M-N") 'next-buffer)


;; Better replacement for C-x o
(use-package switch-window
  :delight
  :ensure t
  :init
  :config
  (global-set-key (kbd "C-x o") 'switch-window)
  ;; (global-set-key (kbd "C-x 1") 'switch-window-then-maximize)
  ;; (global-set-key (kbd "C-x 2") 'switch-window-then-split-below)
  ;; (global-set-key (kbd "C-x 3") 'switch-window-then-split-right)
  (global-set-key (kbd "C-x 0") 'switch-window-then-delete)
  (global-set-key (kbd "C-x 4 d") 'switch-window-then-dired)
  (global-set-key (kbd "C-x 4 f") 'switch-window-then-find-file)
  (global-set-key (kbd "C-x 4 m") 'switch-window-then-compose-mail)
  (global-set-key (kbd "C-x 4 r") 'switch-window-then-find-file-read-only)

  (global-set-key (kbd "C-x 4 C-f") 'switch-window-then-find-file)
  (global-set-key (kbd "C-x 4 C-o") 'switch-window-then-display-buffer)

  (global-set-key (kbd "C-x 4 0") 'switch-window-then-kill-buffer))


;;__________________________________Buffer-move to switch contents of 2 windows
;; https://stackoverflow.com/questions/1774832/how-to-swap-the-buffers-in-2-windows-emacsn
(use-package buffer-move
  :delight
  :ensure t
  :defer t)

;;_____________________________________________Zoom (to enlarge focused window)
(use-package zoom
  :delight
  :ensure t
  :init
  ;; (global-set-key (kbd "<f12>") 'zoom-mode)
  (custom-set-variables '(zoom-size '(0.618 . 0.618)))
  ;; (custom-set-variables '(zoom-size '(0.618 . 0.618)))
  )

;;__________________Zoom-window (make some windows fullscreen and restore back)
(use-package zoom-window
  :delight
  :ensure t
  :init
  (global-set-key (kbd "C-z") 'zoom-window-zoom)
  (custom-set-variables
   '(zoom-window-mode-line-color "LightBlue")))

;; TODO add zoom-window + multi-buffer follow mode
;; 1. Zoom in on current window
;; 2. fill screen with 80 width columns
;; 3. activate follow mode
;; calling zoom again should quit

;;___________persperctive: Setup perspectives/workspaces to switch between them
;; https://www.youtube.com/watch?v=I28jFkpN5Zk
(use-package perspective
  :delight
  :ensure t
  :defer t
  :init
  (persp-mode t)
)

;;__________________________________________________________________PARANTHESES

;;_____________________________________________________________Smartparens mode
;; https://github.com/Fuco1/smartparens
;; https://ebzzry.io/en/emacs-pairs/
(use-package smartparens
  :delight
  :ensure t)

(use-package smartparens-config
  :ensure smartparens
  :defer t
  :init
  (add-hook 'prog-mode-hook 'turn-on-smartparens-strict-mode)
  (add-hook 'markdown-mode-hook 'turn-on-smartparens-strict-mode)
  :config
  (progn (show-smartparens-global-mode t))
  (bind-keys
   :map smartparens-mode-map
   ("C-M-a" . sp-beginning-of-sexp)
   ("C-M-e" . sp-end-of-sexp)

   ("C-<down>" . sp-down-sexp)
   ("C-<up>"   . sp-up-sexp)
   ("M-<down>" . sp-backward-down-sexp)
   ("M-<up>"   . sp-backward-up-sexp)

   ("C-M-f" . sp-forward-sexp)
   ("C-M-b" . sp-backward-sexp)

   ("C-M-n" . sp-next-sexp)
   ("C-M-p" . sp-previous-sexp)

   ("C-S-f" . sp-forward-symbol)
   ("C-S-b" . sp-backward-symbol)

   ("C-<right>" . sp-forward-slurp-sexp)
   ("M-<right>" . sp-forward-barf-sexp)
   ("C-<left>"  . sp-backward-slurp-sexp)
   ("M-<left>"  . sp-backward-barf-sexp)

   ("C-M-t" . sp-transpose-sexp)
   ("C-M-k" . sp-kill-sexp)
   ("C-k"   . sp-kill-hybrid-sexp)
   ("M-k"   . sp-backward-kill-sexp)
   ("C-M-w" . sp-copy-sexp)
   ("C-M-d" . delete-sexp)

   ("M-<backspace>" . backward-kill-word)
   ("C-<backspace>" . sp-backward-kill-word)
   ([remap sp-backward-kill-word] . backward-kill-word)

   ("M-[" . sp-backward-unwrap-sexp)
   ("M-]" . sp-unwrap-sexp)

   ("C-x C-t" . sp-transpose-hybrid-sexp)

   ("C-c ("  . wrap-with-parens)
   ("C-c ["  . wrap-with-brackets)
   ("C-c {"  . wrap-with-braces)
   ("C-c '"  . wrap-with-single-quotes)
   ("C-c \"" . wrap-with-double-quotes)
   ("C-c _"  . wrap-with-underscores)
   ("C-c `"  . wrap-with-back-quotes)))

;;_______________rainbow-delimiters to not get lost inside too many paranthesis
;; (specifically for lisp things)
(use-package rainbow-delimiters
  :delight rainbow-mode
  :ensure t
  :defer t
  :init
  (add-hook 'lisp-mode-hook (lambda () (rainbow-delimiters-mode)))
  :config)

(use-package highlight-parentheses
  :delight
  :ensure t
  :defer t
  :init
  (add-hook 'emacs-lisp-mode-hook (lambda () (highlight-parentheses-mode))))


;;________________________________________________________________________OTHER

(use-package abbrev
  :delight abbrev-mode)

;;__________________________;; LSP (Language server protocol) adds IDE features
(use-package lsp-mode
  :delight
  :ensure t
  :defer t
  ;; :commands lsp
  :init
  ;; by default lsp mode has too many features so do configure it manually
  (setq lsp-auto-configure nil)
  ;; disable variable naming etc..
  (setq lsp-enable-xref nil)
  ;; don't mess with indentation!
  (setq lsp-enable-indentation nil)
  ;; don't know what this is
  (setq lsp-enable-on-type-formatting nil)
  ;; enable snippets (need to configure yasnippet as well - done below)
  (setq lsp-enable-snippet t)
  ;; enable completion-at-point (need to add lsp to company-backends)
  (setq lsp-enable-completion-at-point t)
  ;; enable eldoc signature help
  (setq lsp-eldoc-enable-hover t)
  ;; (require 'helm-lsp)
  :bind (("M-." . lsp-find-definition)
		 ;; TODO wait for melpa support then move these to .emacs-helm.el
		 ;; ("M-," . helm-lsp-find-references)
		 ("M-," . lsp-find-references)))

(use-package lsp-ui
  :delight
  :ensure lsp-mode
  :ensure t
  :defer t
  ;; :commands lsp-ui-mode
  ;; this mode pollutes screen too much but worth exploring
  ;; disabled with (setq lsp-auto-configure nil)
  :init)

;;____________________________________________________________________Yasnippet
;; To expand predefined shortcuts
(use-package yasnippet
  :delight
  :ensure t
  :defer t
  :init
  ;; (add-hook 'c++-mode-hook 'yas-minor-mode)
  ;; (add-hook 'c-mode-hook 'yas-minor-mode)
  ;; (add-hook 'python-mode-hook 'yas-minor-mode)
  ;; (add-hook 'org-mode-hook 'yas-minor-mode)
  ;; (add-hook 'TeX-mode-hook 'yas-minor-mode)
  (yas-global-mode t))


;;___________________________________________________________________Completion
;; http://cachestocaches.com/2015/8/c-completion-emacs/
(use-package company
  :delight
  :ensure t
  :defer t
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  (add-hook 'emacs-lisp-mode-hook 'company-mode)
  (add-hook 'org-mode-hook 'company-mode)

  ;;__________________________________To make company behave like quto-complete
  ;; https://github.com/company-mode/company-mode/wiki/Switching-from-AC
  ;; (defun my-company-visible-and-explicit-action-p ()
  ;;   (and (company-tooltip-visible-p)
  ;;        (company-explicit-action-p)))

  ;; (defun company-ac-setup ()
  ;;   "Sets up `company-mode' to behave similarly to `auto-complete-mode'."
  ;;   (setq company-require-match nil)
  ;;   (setq company-auto-complete #'my-company-visible-and-explicit-action-p)
  ;;   (setq company-frontends '(company-echo-metadata-frontend
  ;;                             company-pseudo-tooltip-unless-just-one-frontend-with-delay
  ;;                             company-preview-frontend))
  ;;   (define-key company-active-map [tab]
  ;;     'company-select-next-if-tooltip-visible-or-complete-selection)
  ;;   (define-key company-active-map (kbd "TAB")
  ;;     'company-select-next-if-tooltip-visible-or-complete-selection))

  :config
  (setq company-idle-delay 0
        company-minimum-prefix-length 3
        company-show-numbers t
        company-tooltip-limit 20
        company-tooltip-align-annotations t
        company-dabbrev-downcase nil
        company-selection-wrap-around t
        ;; company-auto-complete 'company-explicit-action-p
        company-backends '((company-elisp company-lsp company-auctex)))

  :bind (("C-," . company-complete-common)
         :map company-active-map
         ("C-n" . company-select-next)
         ("C-p" . company-select-previous)
         ("C-e" . company-complete-common)))

;;_____________________________________Show documentation next to company popup
;; TODO this misbehaves with qtile?!?
;; (use-package company-quickhelp
;;   :after company
;;   :ensure company
;;   :ensure t
;;   :defer t
;;   :delight
;;   :init
;;   (company-quickhelp-mode)
;;   (setq company-quickhelp-delay 1)
;;   (eval-after-load 'company
;;     '(define-key company-active-map (kbd "C-c h") #'company-quickhelp-manual-begin)))

;;________________________________________________Company mode fuzzy completion
;; TODO uninstall maybe (not working well?)
;; (use-package company-flx
;;   :after company
;;   :ensure company
;;   :ensure t
;;   :defer t
;;   :init
;;   (with-eval-after-load 'company (company-flx-mode +1)))

;; TODO try company-slime for elisp fuzzy matching

;;________________________________________________Company mode latex completion
(use-package company-auctex
  :after company
  :ensure company
  :ensure t
  :defer t
  :config
  (company-auctex-init))


(use-package company-lsp
  :after company
  :ensure company
  :ensure t
  :defer t
  :commands company-lsp)

;;_______________________________________________For on-the-fly syntax checking
;; http://syamajala.github.io/c-ide.html
(use-package flycheck
  :delight
  :ensure t
  :defer t
  :init
  )

(use-package flymake
  :delight
  :ensure t
  :defer t
  :init
  )

;;_______________________________________________________________Webkit browser
;; (used primarily for cppreference local documentation and helm-dash)
;; https://www.reddit.com/r/emacs/comments/4srze9/watching_youtube_inside_emacs_25/
(use-package xwidgete
  :delight
  :ensure t
  :init
  (unless (file-exists-p "~/.emacs.d/emacs-webkit")
  	(shell-command "git clone https://github.com/martyr-deepin/emacs-webkit.git")
  	(copy-directory "emacs-webkit" "~/.emacs.d/emacs-webkit")
  	(delete-directory "emacs-webkit" t))
  (add-to-list 'load-path "~/.emacs.d/emacs-webkit/")
  :config
  (define-key xwidget-webkit-mode-map (kbd "C-p") 'xwidget-webkit-scroll-down)
  (define-key xwidget-webkit-mode-map (kbd "C-n") 'xwidget-webkit-scroll-up)
  (define-key xwidget-webkit-mode-map (kbd "C-b") 'xwidget-webkit-scroll-backward)
  (define-key xwidget-webkit-mode-map (kbd "C-f") 'xwidget-webkit-scroll-forward))

;;____________________________change default application of browse-url function
;; https://www.emacswiki.org/emacs/BrowseUrl
(setq browse-url-generic-program "webmacs")
(setq browse-url-browser-function 'browse-url-generic)

;;_____________________________________________Projectile for managing projects
(use-package projectile
  :delight
  ;; smart-mode-line already displays project name, so no need for this
  ;; :delight '(:eval (concat " " (projectile-project-name)))
  :ensure t
  :defer t
  :init
  ;: enable caching unconditionally
  (setq projectile-enable-caching t)
  (add-hook 'c-mode-common-hook 'projectile-mode)
  (add-hook 'python-mode-hook 'projectile-mode)
  :config
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  ;; Do not prompt for compilation command
  ;; (might be better to just add this into .dir-locals.el)
  (setq compilation-read-command nil))


;;_____________________________________________________________Semantic package
;; to display namespace, function, class name where cursor currently resides in
;; http://tuhdo.github.io/c-ide.html
(use-package semantic
  :delight
  :ensure t
  :defer t
  :init
  (add-to-list 'semantic-default-submodes 'global-semantic-stickyfunc-mode)
  ;; using ycmd-eldoc instead
  ;; (add-to-list 'semantic-default-submodes
  ;;              'global-semantic-idle-summary-mode)
  (semantic-mode 1)
  :config
  ;; Not using this anymore (using ycmd-eldoc instead)
  ;; (global-semantic-idle-summary-mode 1)
  (global-semantic-idle-scheduler-mode 1)
  (global-semanticdb-minor-mode 1))

(use-package stickyfunc-enhance
  :after semantic
  :ensure semantic
  :ensure t
  :defer t
  :delight stickyfunc-enhance
  :init
  (require 'stickyfunc-enhance)
  )

;;________________________________________________________________Git interface
(use-package magit
  :delight
  :ensure t
  :defer t
  :init
  (setq magit-view-git-manual-method 'man)
  (bind-keys
   ("C-x g" . magit-status))
  )

;;___________________________________Anki editor integration (not using though)
(use-package anki-editor
  :delight
  :ensure t
  :init)

;;____________________________________________________To edit fish script files
(use-package fish-mode
  :delight
  :ensure t
  :init)

;;____________________________________________________________To edit lua files
(use-package lua-mode
  :delight
  :ensure t
  :init)

;;____________________________________________Embed pyqt5 applications in Emacs
(when (not (file-exists-p "~/.emacs.d/emacs-application-framework"))
  (shell-command "git clone https://github.com/manateelazycat/emacs-application-framework.git")
  (shell-command "mv emacs-application-framework ~/.emacs.d/"))
(add-to-list 'load-path "~/.emacs.d/emacs-application-framework")
(require 'eaf)

;;____________________________________________Include other configuration files

;; Python configuration
(load-file "~/.emacs-python.el")

;; Java configuration
(load-file "~/.emacs-java.el")

;; C/C++ configuration
(load-file "~/.emacs-cpp.el")

;; Haskell configuration
(load-file "~/.emacs-haskell.el")

;; completion & selection framework
(load-file "~/.emacs-helm.el")

;; MacOS configuration
(if (string-equal system-type "darwin") (load-file "~/.emacs-macOS.el") )

;; some ugly functions
(load-file "~/.emacs-ugly.el")

;;______________________________________________________________________________

(provide '.emacs)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-safe-themes
   (quote
    ("bf798e9e8ff00d4bf2512597f36e5a135ce48e477ce88a0764cfb5d8104e8163" "732b807b0543855541743429c9979ebfb363e27ec91e82f463c91e68c772f6e3" "80930c775cef2a97f2305bae6737a1c736079fdcc62a6fdf7b55de669fbbcd13" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" "196df8815910c1a3422b5f7c1f45a72edfa851f6a1d672b7b727d9551bb7c7ba" "36746ad57649893434c443567cb3831828df33232a7790d232df6f5908263692" "ef403aa0588ca64e05269a7a5df03a5259a00303ef6dfbd2519a9b81e4bce95c" "12670281275ea7c1b42d0a548a584e23b9c4e1d2dabb747fd5e2d692bcd0d39b" "8150ded55351553f9d143c58338ebbc582611adc8a51946ca467bd6fa35a1075" "cc0dbb53a10215b696d391a90de635ba1699072745bf653b53774706999208e3" "4780d7ce6e5491e2c1190082f7fe0f812707fc77455616ab6f8b38e796cbffa9" "6145e62774a589c074a31a05dfa5efdf8789cf869104e905956f0cbd7eda9d0e" "bc4c89a7b91cfbd3e28b2a8e9e6750079a985237b960384f158515d32c7f0490" "3e335d794ed3030fefd0dbd7ff2d3555e29481fe4bbb0106ea11c660d6001767" "943100987bf212cab6ff8756ff322d0a7bd1ca1ea06ec4b46dd139780e1814b7" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "36c3457b6b364cc93f2d52e2d9b9ec32bab75b27f5bd2394757657f18f4d324e" "9f08dacc5b23d5eaec9cccb6b3d342bd4fdb05faf144bdcd9c4b5859ac173538" "36282815a2eaab9ba67d7653cf23b1a4e230e4907c7f110eebf3cdf1445d8370" "36ca8f60565af20ef4f30783aa16a26d96c02df7b4e54e9900a5138fb33808da" "99c86852decaeb0c6f51ce8bd46e4906a4f28ab4c5b201bdc3fdf85b24f88518" "fede08d0f23fc0612a8354e0cf800c9ecae47ec8f32c5f29da841fe090dfc450" "47ec21abaa6642fefec1b7ace282221574c2dd7ef7715c099af5629926eb4fd7" "dba2c5f513fa12602c2c3305cef28565e6b54db357a664a49ef6919e71732d9e" "0f1733ad53138ddd381267b4033bcb07f5e75cd7f22089c7e650f1bb28fc67f4" "37c5cf50a60548aa7e01dbe36fd8bb643af7502d55d26f000070255a6b21c528" "bd0c3e37c53f6515b5f5532bdae38aa0432a032171b415159d5945a162bc5aaa" "030346c2470ddfdaca479610c56a9c2aa3e93d5de3a9696f335fd46417d8d3e4" "97b8bf2dacc3ae8ffbd6f0a76c606a659a0dbca5243e55a750cbccdad7efb098" "bf39b2d814971a6eaf4e9adde3b11016b742fe68dfe4c38667497821525a1662" "8abc02cbf62cc9efb0e571233387866b9b26c3c4e8ab75148b502f0646b46225" "79dc2bcd864232143f961d46454c2252bb705b691967b61558e038e8497ff2e5" "bee55ba5e878d0584db9b2fb33f75c348a3008fcfe8e05ab8cae897ca604fd95" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" "02199888a97767d7779269a39ba2e641d77661b31b3b8dd494b1a7250d1c8dc1" "8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "174502267725776b47bdd2d220f035cae2c00c818765b138fea376b2cdc15eb6" "0c3b1358ea01895e56d1c0193f72559449462e5952bded28c81a8e09b53f103f" "ecfd522bd04e43c16e58bd8af7991bc9583b8e56286ea0959a428b3d7991bbd8" "c9ddf33b383e74dac7690255dd2c3dfa1961a8e8a1d20e401c6572febef61045" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(eclim-eclipse-dirs (quote ("/opt/eclipse")))
 '(eclim-executable "/opt/eclipse/eclim")
 '(eclimd-default-workspace "~/workspace-eclipse")
 '(eclimd-executable "/opt/eclipse/eclimd")
 '(global-visual-line-mode t)
 '(package-selected-packages
   (quote
    (ranger xwidgete helm-dash helm-gtags helm-rg company-emacs-eclim paradox ag helm-ag helm-rhythmbox which-key use-package stickyfunc-enhance smartparens smart-tabs-mode rainbow-delimiters multiple-cursors highlight-parentheses helm-swoop helm-rtags helm-projectile helm-make helm-descbinds helm-company helm-c-moccur gradle-mode golden-ratio ggtags eclim company-quickhelp company-auctex cmake-ide cmake-font-lock buffer-move)))
 '(paradox-github-token t)
 '(safe-local-variable-values
   (quote
    ((projectile-project-compilation-cmd . "make")
     (projectile-project-compilation-dir . "./debug")
     (compilation-dir . "./debug")
     (projectile-project-name . "spgemm-bandwidth")
     (projectile-project-root . /home/macbeth/workspace-linalg/kokkos-kernels/)
     (cmake-ide-build-dir . "./debug"))))
 '(zoom-size (quote (0.618 . 0.618)))
 '(zoom-window-mode-line-color "DarkGreen"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'dired-find-alternate-file 'disabled nil)
