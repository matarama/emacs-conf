
;;_______________________________________________Python development environment
(use-package elpy
  :delight
  :ensure t
  :ensure helm-dash
  :defer t
  :init
  (add-to-list 'auto-mode-alist '("\\.py$" . python-mode))
  (add-hook 'python-mode-hook #'elpy-mode)
  :bind (:map elpy-mode-map
              ("<M-left>" . nil)
              ("<M-right>" . nil)
              ("<M-S-left>" . elpy-nav-indent-shift-left)
              ("<M-S-right>" . elpy-nav-indent-shift-right)
              ("M-." . elpy-goto-definition)
              ("M-," . pop-tag-mark))
  :config
  ;; (setq elpy-rpc-backend "pope")
  (setq elpy-rpc-backend "jedi"))

(use-package python
  :mode ("\\.py" . python-mode)
  :init
  (setq python-indent-offset 4)
  (add-hook 'python-mode-hook (lambda () (setq indent-tabs-mode nil))))

(use-package pyenv-mode
  :init
  (add-to-list 'exec-path "~/.pyenv/shims")
  (setenv "WORKON_HOME" "~/.pyenv/versions/")

  (defun pyenv-activate-current-project ()
	"Automatically activates pyenv version if .python-version file exists."
	(interactive)
	(let ((python-version-directory
           (locate-dominating-file (buffer-file-name) ".python-version")))
      (if python-version-directory
          (let* ((pyenv-version-path
                  (f-expand ".python-version" python-version-directory))
				 (pyenv-current-version
                  (s-trim (f-read-text pyenv-version-path 'utf-8))))
			(pyenv-mode-set pyenv-current-version)
			(message (concat "Setting virtualenv to "
							 pyenv-current-version))))))

  (defvar pyenv-current-version nil nil)
  (defun pyenv-init()
	"Initialize pyenv's current version to the global one."
	(let ((global-pyenv
		   (replace-regexp-in-string
			"\n" "" (shell-command-to-string "pyenv global"))))
      (message (concat "Setting pyenv version to " global-pyenv))
	  ;; (pyenv-init)
      (pyenv-mode-set global-pyenv)
      (setq pyenv-current-version global-pyenv)))

  ;; (add-hook 'after-init-hook 'pyenv-init)
  :config
  (pyenv-mode)

  :bind
  ("C-x p e" . pyenv-activate-current-project))

;;________________________________________________________________DOCUMENTATION

;; After installing docsets for Python 3 (and 2) make a symbolic link to
;; Python_3 (and Python_2). That is:
;; cd ~/.docsets
;; ln -s Python\ 3.docset Python_3.docset
;; ln -s Python\ 2.docset Python_2.docset
(when (require 'helm-dash nil 'noerror)
  (helm-dash-ensure-docset-installed "Python_3")
  (helm-dash-ensure-docset-installed "Python_2")
  (helm-dash-ensure-docset-installed "SciPy")
  (helm-dash-ensure-docset-installed "NumPy")
  (helm-dash-ensure-docset-installed "Matplotlib")

  (if (and (not (file-exists-p "~/.docsets/Python_3.docset"))
		   (file-exists-p "~/.docsets/Python 3.docset"))
	  (shell-command "ln -s ~/.docsets/Python\ 3.docset ~/.docsets/Python_3.docset"))

  (if (and (not (file-exists-p "~/.docsets/Python_2.docset"))
		   (file-exists-p "~/.docsets/Python 2.docset"))
	  (shell-command "ln -s ~/.docsets/Python\ 2.docset ~/.docsets/Python_2.docset"))

  (add-hook 'python-mode-hook
            (lambda () (helm-dash-activate-docset "Python_3")))
  (add-hook 'python-mode-hook (lambda () (helm-dash-activate-docset "SciPy")))
  (add-hook 'python-mode-hook (lambda () (helm-dash-activate-docset "NumPy")))
  (add-hook 'python-mode-hook
            (lambda () (helm-dash-activate-docset "Matplotlib"))))
