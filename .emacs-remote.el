;; Add Melpa to repository list
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa"	.	"http://melpa.org/packages/")
   t)
  (package-initialize))

;; debug on error
;; (setq debug-on-error t)
;;___________________________________________________________ASTHETICS/BEHAVIOR
;; don't show tool-bar, scroll-bar, and menu-bar
;; (tool-bar-mode -1)
;; (scroll-bar-mode -1)
(menu-bar-mode -1)

;; set font size
;; (set-face-attribute 'default nil :height 110)
;; (set-face-attribute 'default nil :font "Ubuntu Mono")

;; unicode characters
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; System locale to use for formatting time values.
(setq system-time-locale "C")

;: automatically refresh all files when updated
(global-auto-revert-mode t)

;; Do not highlight marked region
(setq transient-mark-mode nil)

;; Make startup screen scratch buffer
(setq inhibit-startup-screen t)

;; highlight matching parenthesis
(show-paren-mode 1)

;; lines wrapped around beautifuly
(global-visual-line-mode 1)

;; highlight the line cursor is on
;; (global-hl-line-mode 1)

;; How to make dired use the same buffer for viewing directory?
;; http://ergoemacs.org/emacs/emacs_dired_tips.html
(require 'dired )
(define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file) ; was dired-advertised-find-file
(define-key
  dired-mode-map (kbd "^")
  (lambda () (interactive) (find-alternate-file "..")))  ;; was dired-up-directory

;; eshell shortcut
(global-set-key "\C-xt" 'eshell)

;; make compilation buffer scroll down
(setq compilation-scroll-output t)
(setq compilation-scroll-output 'first-error)

;; if a buffer is opened in other frame, reuse it
(setq display-buffer-reuse-frames t)

;; http://ergoemacs.org/emacs/emacs_alias.html
(defalias 'yes-or-no-p 'y-or-n-p) ; y or n is enough
(defalias 'list-buffers 'ibuffer) ; always use ibuffer

;; Remove trailing whitespace
;; https://www.emacswiki.org/emacs/DeletingWhitespace#toc3
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; https://sriramkswamy.github.io/dotemacs/
;; Move correctly over camelCased words
(subword-mode 1)
(global-set-key "\M-f" 'subword-right)
(global-set-key "\M-b" 'subword-left)

;; Emacs thinks a sentence is a full-stop followed by 2 spaces.
;; Let’s make it full-stop and 1 space.
(setq sentence-end-double-space nil)
;; make gdb multi-windowed
(setq gdb-many-windows t
      gdb-show-main t)
;; ??
(setq ediff-window-setup-function 'ediff-setup-windows-plain
      ediff-split-window-function 'split-window-horizontally)
;; Narrow to region
(put 'narrow-to-region 'disabled nil)

;; Install use-package if it is not installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(require 'bind-key)

;;___________________________________________________Restart emacs within emacs
(use-package restart-emacs
  :ensure t
  :bind* (("C-x M-c" . restart-emacs)))

;;____________________________________________________To define sticky bindings
(use-package hydra
  :ensure t
  :defer t
  :delight
  :init
  (defun my/reverse-undo ()
    "Reverses undo (does nothing)"
    (message "Undo reversed."))

  (defhydra hydra-undo-redo (global-map "C-x")
    "undo/reverse-undo"
    ("u" undo)
    ("r" (my/reverse-undo))))

;;______________________________________________Clean up mode line with delight
(use-package delight
  :ensure t)

;; enable some minor modes
(use-package emacs
  :delight
  (visual-line-mode)
  (eldoc-mode))

;;___________________________________________________________Choose color theme
(use-package color-theme-sanityinc-tomorrow
  :ensure t
  :defer t
  :init
  ;; (load-theme 'sanityinc-tomorrow-night t)
  :config)

(use-package ample-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'ample t)
  :config)

(use-package base16-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'base16-classic-dark t)
  :config)

(use-package color-theme-solarized
  :ensure t
  :defer t
  :init
  (load-theme 'solarized t)
  :config)

(use-package nimbus-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'nimbus t)
  :config)

(use-package darktooth-theme
  :ensure t
  :defer t
  :init
  ;; (load-theme 'darktooth t)
  :config)

;;____________________________________________________Move to beginning of line
;; http://emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/
(defun my/smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; remap C-a to `smarter-move-beginning-of-line'
;; (global-set-key [remap move-beginning-of-line]
;;                 'my/smarter-move-beginning-of-line)
(global-set-key "\C-a" 'my/smarter-move-beginning-of-line)

;;________________________________________________describe this point lisp only
;; https://www.emacswiki.org/emacs/DescribeThingAtPoint
(defun my/describe-foo-at-point ()
  "Show the documentation of the Elisp function and variable near point.
	This checks in turn:
	-- for a function name where point is
	-- for a variable name where point is
	-- for a surrounding function call
	"
  (interactive)
  (let (sym)
	;; sigh, function-at-point is too clever.  we want only the first half.
	(cond ((setq sym (ignore-errors
                       (with-syntax-table emacs-lisp-mode-syntax-table
                         (save-excursion
                           (or (not (zerop (skip-syntax-backward "_w")))
                               (eq (char-syntax (char-after (point))) ?w)
                               (eq (char-syntax (char-after (point))) ?_)
                               (forward-sexp -1))
                           (skip-chars-forward "`'")
        	               (let ((obj (read (current-buffer))))
                             (and (symbolp obj) (fboundp obj) obj))))))
           (describe-function sym))
          ((setq sym (variable-at-point)) (describe-variable sym))
          ;; now let it operate fully -- i.e. also check the
          ;; surrounding sexp for a function call.
          ((setq sym (function-at-point)) (describe-function sym)))))

(global-set-key (kbd "<f12>") 'my/describe-foo-at-point)

(defun my/find-foo-at-point ()
  "Jump to Elisp function and variable near point.
	This checks in turn:
	-- for a function name where point is
	-- for a variable name where point is
	-- for a surrounding function call
	"
  (interactive)
  (let (sym)
	;; sigh, function-at-point is too clever.  we want only the first half.
	(cond ((setq sym (ignore-errors
                       (with-syntax-table emacs-lisp-mode-syntax-table
                         (save-excursion
                           (or (not (zerop (skip-syntax-backward "_w")))
                               (eq (char-syntax (char-after (point))) ?w)
                               (eq (char-syntax (char-after (point))) ?_)
                               (forward-sexp -1))
                           (skip-chars-forward "`'")
        	               (let ((obj (read (current-buffer))))
                             (and (symbolp obj) (fboundp obj) obj))))))
           (find-function sym))
          ((setq sym (variable-at-point)) (find-variable sym))
          ;; now let it operate fully -- i.e. also check the
          ;; surrounding sexp for a function call.
          ((setq sym (function-at-point)) (find-function sym)))))

(defun my/find-foo-at-point-wrapper()
  (interactive)
  (let ((caller-window))
    (setq caller-window (selected-window))
    (when (> (length (window-list)) 1)
      (switch-window)
      (my/find-foo-at-point)
      ;; (select-window caller-window)
      )
    (when (< (length (window-list)) 2)
      (my/find-foo-at-point))))

(define-key emacs-lisp-mode-map (kbd "M-.") 'my/find-foo-at-point-wrapper)

;;_____________________make backup to a designated dir, mirroring the full path
;; http://ergoemacs.org/emacs/emacs_set_backup_into_a_directory.html
(defun my/backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* ((backupRootDir "~/.emacs.d/emacs-backup/")
         ;; remove Windows driver letter in path, for example, “C:”
         (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath ))
         (backupFilePath (replace-regexp-in-string
                          "//" "/" (concat backupRootDir filePath "~"))))
    (make-directory (file-name-directory backupFilePath)
                    (file-name-directory backupFilePath))
    backupFilePath)
  )

(setq make-backup-file-name-function 'my/backup-file-name)

;;__________________________________________renames current buffer and its file
;; source: http://steve.yegge.googlepages.com/my-dot-emacs-file
(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

;;_____________________________________when window is split, move to new window
(defun my/split-window-right ()
  "Splits window vertically and moves the focus to new window."
  (interactive)
  (split-window-right)
  (other-window 1))

(defun my/split-window-below ()
  "Splits window vertically and moves the focus to new window."
  (interactive)
  (split-window-below)
  (other-window 1))

(global-set-key (kbd "C-x 3") 'my/split-window-right)
(global-set-key (kbd "C-x 2") 'my/split-window-below)

;;_____________________Faster navigation than next-line slower than scroll-down

(defun my/next-line ()
  "Call next-line function multiple times."
  (interactive)
  (let ((c 0))
	(while (< c 5)
	  (setq c (+ 1 c))
	  (next-line))))

(defun my/previous-line ()
  "Call previous-line function multiple times."
  (interactive)
  (let ((c 0))
	(while (< c 5)
	  (setq c (+ 1 c))
	  (previous-line))))


;; (global-set-key (kbd "M-n") 'my/next-line)
;; (global-set-key (kbd "M-p") 'my/previous-line)

;;_____________________________________Scroll up/down without moving the cursor
(global-set-key (kbd "M-N") 'scroll-up-line)
(global-set-key (kbd "M-P") 'scroll-down-line)

;;___________________________(move selected region / yank line) to other window
;; https://emacs.stackexchange.com/questions/3743/how-to-move-region-to-other-window
(defun move-region-to-other-window (start end)
  "Move selected text to other window"
  (interactive "r")
  (if (use-region-p)
      (let ((count (count-words-region start end)))
        (save-excursion
          (kill-region start end)
          (other-window 1)
          (yank)
          (newline))
        (other-window -1)
        (message "Moved %s words" count))
    (message "No region selected")))

(defun kill-line-to-other-window ()
  "Kill line and yank it to other window"
  (interactive)
  (kill-line)
  (other-window 1)
  (yank)
  (newline)
  (other-window -1))

(global-set-key [(control 252)] 'kill-line-to-other-window)

;;__Most Eshell commands are available only after Eshell has been started once.
;; https://github.com/emacs-helm/helm/wiki/Find-Files
(add-hook 'emacs-startup-hook
          (lambda ()
            (let ((default-directory (getenv "HOME")))
              (command-execute 'eshell)
              ;; Commands that need a pager/curses
              (add-to-list 'eshell-visual-commands "top")
              (add-to-list 'eshell-visual-commands "htop")
              (add-to-list 'eshell-visual-options '("git" "--help"))
              (add-to-list 'eshell-visual-subcommands
                           '("git" "log" "diff" "show" ))
              (bury-buffer))))


;;________________________________Never lose cursor when switching buffers etc.
(use-package beacon
  :delight
  :ensure t
  :config
  (beacon-mode 1))

;;_____________________________________________________________Better mode line
(use-package smart-mode-line
  :delight
  :ensure t
  :init
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'respectful)
  (sml/setup)
  ;; (use-package smart-mode-line-powerline-theme
  ;; :ensure t
  ;; :config
  ;; (setq sml/theme 'powerline))
  )

;;_________________________________________________________________________TABS

;; Set tab size
(setq-default c-basic-offset 4
              tab-width 4
              ;; indent-tabs-mode t
              indent-tabs-mode nil)
;; (add-hook 'emacs-lisp-mode-hook (lambda () (setq indent-tabs-mode nil)))

;; Use both TABs and SPACEs
;; https://www.emacswiki.org/emacs/SmartTabs
;; ----------------------------------------------------------------------------
;; (use-package smart-tabs-mode
;;   :delight smart-tab-mode
;;   :ensure t
;;   :defer t
;;   :init
;;   (add-hook 'c-mode-common-hook (lambda () (setq indent-tabs-mode t)))
;;   (smart-tabs-insinuate 'c 'c++ 'java))


;;_______________________________________________________________BASE_FUNCTIONS

;; Copy word at point (used inside other functions)
(defun get-point (symbol &optional arg)
  "get the point"
  (funcall symbol arg)
  (point)
  )

(defun copy-thing (begin-of-thing end-of-thing &optional arg)
  "copy thing between beg & end into kill ring"
  (save-excursion
	(let ((beg (get-point begin-of-thing 1))
          (end (get-point end-of-thing arg)))
	  (copy-region-as-kill beg end)))
  )

(defun paste-to-mark(&optional arg)
  "Paste things to mark, or to the prompt in shell-mode"
  (let ((pasteMe
         (lambda()
           (if (string= "shell-mode" major-mode)
               (progn (comint-next-prompt 25535) (yank))
             (progn (goto-char (mark)) (yank) )))))
    (if arg
        (if (= arg 1)
            nil
          (funcall pasteMe))
      (funcall pasteMe))
    ))

(defun copy-word (&optional arg)
  "Copy words at point into kill-ring"
  (interactive "P")
  (copy-thing 'backward-word 'forward-word arg)
  ;;(paste-to-mark arg)
  )

;;______________________________________________________________________EDITING

;;___________________Multiple cursors for editing multiple lines simultaneously
;; https://github.com/magnars/multiple-cursors.el
(use-package multiple-cursors
  :delight
  :ensure t
  :defer t
  :bind (("C-C C-C" . mc/edit-lines)
         ("C->" . mc/mark-next-like-this)
         ("C-<" . mc/mark-previous-like-this)
         ("C-c C-<" . mc/mark-all-like-this)))

;;_________________________________from: Emacs Cookbook - put commas in numbers
;; (defun group-number (num &optional size char)
;;   "Format NUM as string grouped to SIZE with CHAR."
;;   ;; Based on code for `math-group-float' in calc-ext.el
;;   (interactive)
;;   (let* ((size (or size 3))
;;          (char (or char ","))
;;          (str (if (stringp num)
;;                   num
;;                 (number-to-string num)))
;;          ;; omitting any trailing non-digit chars
;;          ;; NOTE: Calc supports BASE up to 36 (26 letters and 10 digits ;)
;;          (pt (or (string-match "[^0-9a-zA-Z]" str) (length str))))
;;     (while (> pt size)
;;       (setq str (concat (substring str 0 (- pt size))
;;                         char
;;                         (substring str (- pt size)))
;;             pt (- pt size)))
;;     str))

;;_____________________________________79 character comment from right function
(defun xah-comment-line ()
  "Print number of words and chars in text selection or line.
In emacs 24, you can use `count-words'."
  (interactive)
  (let (mp p1 p2)
    (if (region-active-p)
        (progn (setq p1 (region-beginning))
               (setq p2 (region-end)))
      (progn
        (back-to-indentation)
        (setq p1 (point))
        (setq p2 (line-end-position))))

    (save-excursion
      (let (charCnt)
        (back-to-indentation)
        (setq mp (point))
        (setq charCnt ( - p2 p1 ))
        (insert-char ?\; 2)
        (setq charCnt (- p2 p1))
        (while (< (current-column) ( - 79 charCnt ))
          (insert-char ?_))))))

;;_______________________79 character comment from right function (c/java ver.)
(defun my/coding/javac-section ()
  "Print number of words and chars in text selection or line.
In emacs 24, you can use `count-words'."
  (interactive)
  (let (mp p1 p2)
    (if (region-active-p)
        (progn (setq p1 (region-beginning))
               (setq p2 (region-end)))
      (progn
        (back-to-indentation)
        (setq p1 (point))
        (setq p2 (line-end-position))))

    (save-excursion
      (let (charCnt)
        (back-to-indentation)
        (setq mp (point))
        (setq charCnt ( - p2 p1 ))
        (insert-char ?\\ 2)
        (setq charCnt (- p2 p1))
        (while (< (current-column) ( - 79 charCnt ))
          (insert-char ?_))))))

;;_____________________________________________Seperator comment for c/c++/java
(defun my/coding/javac-separator ()
  "Insert following c++ command in current buffer (79 character):
// ----------------------------------------------------------------------------

Note that length of the comment will be 79 characters together with
current column. For example>
int a = 4;// ------------------------------------------------------------------
"
  (interactive)
  (let ((amount-to-pad (- 76 (current-column))))
    (when (> amount-to-pad 0)
      (message "current-indentation: %d" (current-indentation))
      (insert "// ")
      (insert-char ?- amount-to-pad))))



;;____________________________________________________NAVIGATION/FRAMES/WINDOWS

;;_________Use isearch in other window while restoring cursor to current buffer
(defun my/isearch-forward/other-window ()
  (interactive)
  (when (> (length (window-list)) 1)
    (save-selected-window
      (select-window (next-window) t)
      (isearch-forward))))

(defun my/isearch-backward/other-window ()
  (interactive)
  (when (> (length (window-list)) 1)
    (save-selected-window
      (select-window (next-window) t)
      (isearch-backward))))

(global-set-key (kbd "C-c s") 'my/isearch-forward/other-window)
(global-set-key (kbd "C-c r") 'my/isearch-backward/other-window)

;;____________________________________________________Move focus between frames
(defun my/next-frame ()
  "Move focus to 'next frame'."
  (interactive)
  (let ((nf (next-frame)))
    (select-frame-set-input-focus nf))
  )

(defun my/previous-frame ()
  "Move focus to 'previous frame'."
  (interactive)
  (let ((pf (previous-frame)))
    (select-frame-set-input-focus pf))
  )

(global-set-key (kbd "M-n") 'my/next-frame)
(global-set-key (kbd "M-p") 'my/previous-frame)

;;______________________________________________move between windows, taken from
;; https://www.emacswiki.org/emacs/WindMove
;; -----------------------------------------------------------------------------
(setq windmove-wrap-around t)
(global-set-key (kbd "C-c k") 'windmove-left)
(global-set-key (kbd "C-c ş") 'windmove-right)
(global-set-key (kbd "C-c o") 'windmove-up)
(global-set-key (kbd "C-c l") 'windmove-down)
(global-set-key [f9] 'previous-buffer)
(global-set-key [f10] 'next-buffer)

;; Better replacement for C-x o
(use-package switch-window
  :delight
  :ensure t
  :init
  :config
  (global-set-key (kbd "C-x o") 'switch-window)
  ;; (global-set-key (kbd "C-x 1") 'switch-window-then-maximize)
  ;; (global-set-key (kbd "C-x 2") 'switch-window-then-split-below)
  ;; (global-set-key (kbd "C-x 3") 'switch-window-then-split-right)
  (global-set-key (kbd "C-x 0") 'switch-window-then-delete)
  (global-set-key (kbd "C-x 4 d") 'switch-window-then-dired)
  (global-set-key (kbd "C-x 4 f") 'switch-window-then-find-file)
  (global-set-key (kbd "C-x 4 m") 'switch-window-then-compose-mail)
  (global-set-key (kbd "C-x 4 r") 'switch-window-then-find-file-read-only)

  (global-set-key (kbd "C-x 4 C-f") 'switch-window-then-find-file)
  (global-set-key (kbd "C-x 4 C-o") 'switch-window-then-display-buffer)

  (global-set-key (kbd "C-x 4 0") 'switch-window-then-kill-buffer))


;;__________________________________Buffer-move to switch contents of 2 windows
;; https://stackoverflow.com/questions/1774832/how-to-swap-the-buffers-in-2-windows-emacsn
(use-package buffer-move
  :delight
  :ensure t
  :defer t)

;;__________________Zoom-window (make some windows fullscreen and restore back)
(use-package zoom-window
  :delight
  :ensure t
  :init
  (global-set-key (kbd "C-z") 'zoom-window-zoom)
  (custom-set-variables
   '(zoom-window-mode-line-color "LightBlue")))

;;___________persperctive: Setup perspectives/workspaces to switch between them
;; https://www.youtube.com/watch?v=I28jFkpN5Zk
(use-package persp-mode
  :delight
  :ensure t
  :defer t
  :init
  (persp-mode t)
)

;;__________________________________________________________________PARANTHESES

;;_____________________________________________________________Smartparens mode
;; https://github.com/Fuco1/smartparens
;; https://ebzzry.io/en/emacs-pairs/
(use-package smartparens
  :delight
  :ensure t)

(use-package smartparens-config
  :ensure smartparens
  :defer t
  :init
  (add-hook 'prog-mode-hook 'turn-on-smartparens-strict-mode)
  (add-hook 'markdown-mode-hook 'turn-on-smartparens-strict-mode)
  :config
  (progn (show-smartparens-global-mode t))
  (bind-keys
   :map smartparens-mode-map
   ("C-M-a" . sp-beginning-of-sexp)
   ("C-M-e" . sp-end-of-sexp)

   ("C-<down>" . sp-down-sexp)
   ("C-<up>"   . sp-up-sexp)
   ("M-<down>" . sp-backward-down-sexp)
   ("M-<up>"   . sp-backward-up-sexp)

   ("C-M-f" . sp-forward-sexp)
   ("C-M-b" . sp-backward-sexp)

   ("C-M-n" . sp-next-sexp)
   ("C-M-p" . sp-previous-sexp)

   ("C-S-f" . sp-forward-symbol)
   ("C-S-b" . sp-backward-symbol)

   ("C-<right>" . sp-forward-slurp-sexp)
   ("M-<right>" . sp-forward-barf-sexp)
   ("C-<left>"  . sp-backward-slurp-sexp)
   ("M-<left>"  . sp-backward-barf-sexp)

   ("C-M-t" . sp-transpose-sexp)
   ("C-M-k" . sp-kill-sexp)
   ("C-k"   . sp-kill-hybrid-sexp)
   ("M-k"   . sp-backward-kill-sexp)
   ("C-M-w" . sp-copy-sexp)
   ("C-M-d" . delete-sexp)

   ("M-<backspace>" . backward-kill-word)
   ("C-<backspace>" . sp-backward-kill-word)
   ([remap sp-backward-kill-word] . backward-kill-word)

   ("M-[" . sp-backward-unwrap-sexp)
   ("M-]" . sp-unwrap-sexp)

   ("C-x C-t" . sp-transpose-hybrid-sexp)

   ("C-c ("  . wrap-with-parens)
   ("C-c ["  . wrap-with-brackets)
   ("C-c {"  . wrap-with-braces)
   ("C-c '"  . wrap-with-single-quotes)
   ("C-c \"" . wrap-with-double-quotes)
   ("C-c _"  . wrap-with-underscores)
   ("C-c `"  . wrap-with-back-quotes)))

;;_______________rainbow-delimiters to not get lost inside too many paranthesis
;; (specifically for lisp things)
(use-package rainbow-delimiters
  :delight rainbow-mode
  :ensure t
  :defer t
  :init
  (add-hook 'lisp-mode-hook (lambda () (rainbow-delimiters-mode)))
  :config)

(use-package highlight-parentheses
  :delight
  :ensure t
  :defer t
  :init
  (add-hook 'emacs-lisp-mode-hook (lambda () (highlight-parentheses-mode))))


;;________________________________________________________________________OTHER

;;___________________________________________________________________Completion
;; http://cachestocaches.com/2015/8/c-completion-emacs/
(use-package company
  :delight
  :ensure t
  :defer t
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  (add-hook 'emacs-lisp-mode-hook 'company-mode)

  :config
  (setq company-idle-delay 0
        company-minimum-prefix-length 3
        company-show-numbers t
        company-tooltip-limit 20
        company-tooltip-align-annotations t
        company-dabbrev-downcase nil
        company-selection-wrap-around t
        company-backends '((company-elisp)))

  :bind (("C-," . company-complete-common)
         :map company-active-map
         ("C-n" . company-select-next)
         ("C-p" . company-select-previous)))

;;_____________________________________________Projectile for managing projects
(use-package projectile
  :delight
  ;; smart-mode-line already displays project name, so no need for this
  ;; :delight '(:eval (concat " " (projectile-project-name)))
  :ensure t
  :defer t
  :init
  ;: enable caching unconditionally
  (setq projectile-enable-caching t)
  (add-hook 'c-mode-common-hook 'projectile-mode)
  (add-hook 'python-mode-hook 'projectile-mode)
  :config
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  ;; Do not prompt for compilation command
  ;; (might be better to just add this into .dir-locals.el)
  (setq compilation-read-command nil))


;;_____________________________________________________________Semantic package
;; to display namespace, function, class name where cursor currently resides in
;; http://tuhdo.github.io/c-ide.html
(use-package semantic
  :delight
  :ensure t
  :defer t
  :init
  (add-to-list 'semantic-default-submodes 'global-semantic-stickyfunc-mode)
  ;; using ycmd-eldoc instead
  ;; (add-to-list 'semantic-default-submodes
  ;;              'global-semantic-idle-summary-mode)
  (semantic-mode 1)
  :config
  ;; Not using this anymore (using ycmd-eldoc instead)
  ;; (global-semantic-idle-summary-mode 1)
  (global-semantic-idle-scheduler-mode 1)
  (global-semanticdb-minor-mode 1))

(use-package stickyfunc-enhance
  :after semantic
  :ensure semantic
  :ensure t
  :defer t
  :delight stickyfunc-enhance
  :init
  (require 'stickyfunc-enhance)
  )

;;________________________________________________________________Git interface
(use-package magit
  :delight
  :ensure t
  :defer t
  :init
  (setq magit-view-git-manual-method 'man)
  (bind-keys
   ("C-x g" . magit-status))
  )

;;____________________________________________Include other configuration files


;;_________________________________________________________Completion framework
;; http://tuhdo.github.io/helm-intro.html
(use-package helm
  :delight
  :ensure t
  :defer t
  :init
  (defun my/helm-buffer-size-increase ()
    "Increase helm buffer size by 10"
    (interactive)
    (setq helm-autoresize-min-height (+ helm-autoresize-min-height 10)))

  (defun my/helm-buffer-size-decrease ()
    "Decrease helm buffer size by 10"
    (interactive)
    (setq helm-autoresize-min-height (- helm-autoresize-min-height 10)))

  (defun my/helm-buffer-size-reset ()
    "Resets helm buffer size to 30%"
    (interactive)
    (setq helm-autoresize-min-height 30))

  (setq helm-use-frame-when-more-than-two-windows nil)
  (add-hook 'c-mode-common-hook (lambda () (helm-mode 1)))
  (helm-mode 1)
  :bind (("M-x" . helm-M-x)
         ("M-y" . helm-show-kill-ring)
         ("C-x C-f" . helm-find-files)
         ("C-x b" . helm-mini)
         ("C-x C-b" . helm-buffers-list)
         ("C-h SPC" . helm-all-mark-rings)
         ("C-c h m" . helm-man-woman)
         ("C-c h a" . helm-apropos)
         ("C-c h i" . helm-semantic-or-imenu)
         ("C-c h /" . helm-find)
         ("C-c h l" . helm-locate)
         ("C-c h <tab>" . helm-lisp-completion-at-point)
         ("C-c h b" . helm-resume)
         ("C-c h r" . helm-regexp)
         ("C-c h t" . helm-top)
         ("C-c h M-." . helm-eval-expression-with-eldoc)
         ("C-c h C-," . helm-calcul-expression)
         :map helm-map
         ;; adjust helm buffer size
         ("C-+" . my/helm-buffer-size-increase)
         ("C--" . my/helm-buffer-size-decrease)
         ("C-." . my/helm-buffer-size-reset)
         ;; rebind tab to run persistent action
         ("<tab>" . helm-execute-persistent-action)
         ;; make TAB work in terminal
         ("C-i" . helm-execute-persistent-action)
         ;; TODO make this work within helm-minibuffer
         ;; ("C-h" . describe-foo-at-point)
         ;; list actions using C-z)
         ("C-z" . helm-select-action)
         ("M-n" . helm-next-source)
         ("M-p" . helm-previous-source)
         :map minibuffer-local-map
         ("C-c C-l" . helm-mini-buffer-history))
  :config
  (require 'helm-config)

  ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
  ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
  ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c"))

  ;; open helm buffer inside current window, not occupy whole other window
  (setq helm-split-window-in-side-p t)

  ;; move to end or beginning of source when reaching top or bottom of source.
  (setq helm-move-to-line-cycle-in-source t)

  ;; search for library in `require' and `declare-function' sexp.
  (setq helm-ff-search-library-in-sexp t)

  ;; scroll 8 lines other window using M-<next>/M-<prior>
  (setq helm-scroll-amount 8)
  (setq helm-ff-file-name-history-use-recentf t)
  (setq helm-echo-input-in-header-line t)
  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 30)
  (helm-autoresize-mode 1)

  ;; Use ack-grep instead of grep
  (when (executable-find "ack-grep")
    (setq helm-grep-default-command
          "ack-grep -Hn --no-group --no-color %e %p %f"
          helm-grep-default-recurse-command
          "ack-grep -H --no-group --no-color %e %p %f"))

  ;; optional fuzzy matching for helm-M-x
  (setq helm-M-x-fuzzy-match t)
  (setq helm-semantic-fuzzy-match t)
  (setq helm-imenu-fuzzy-match t)
  (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)
  (setq helm-apropos-fuzzy-match t)
  (setq helm-buffers-fuzzy-matching t)
  (setq helm-recentf-fuzzy-match t)

  ;; make eshell use helm for auto-complete and command history
  ;; https://github.com/emacs-helm/helm/wiki/Eshell
  (require 'helm-eshell)
  (add-hook 'eshell-mode-hook
            (lambda ()
              (eshell-cmpl-initialize)
              (define-key eshell-mode-map [remap eshell-pcomplete] 'helm-esh-pcomplete)
              (define-key eshell-mode-map (kbd "C-c C-l") 'helm-eshell-history)))
  (defun pcomplete/sudo ()
    (let ((prec (pcomplete-arg 'last -1)))
      (cond ((string= "sudo" prec)
             (while (pcomplete-here*
                     (funcall pcomplete-command-completion-function)
                     (pcomplete-arg 'last) t))))))

  ;; make shell use helm for command history
  (add-hook 'shell-mode-hook
            #'(lambda ()
                (define-key shell-mode-map (kbd "C-c C-l")  'helm-comint-input-ring))))

(use-package helm-rg
  :after helm
  :ensure helm
  :ensure projectile
  :ensure t
  :defer t
  :delight
  :init
  (add-to-list 'load-path "/usr/bin/rg")
  ;; No need use this function.
  ;; helm-projectile already defined a function for this "helm-projectile-rg"
  ;; (defun helm-rg-projectile ()
  ;; 	"Call helm-rg from project's root directory."
  ;; 	(interactive)
  ;; 	(when projectile-mode
  ;; 	  (message "project-root=%s" (projectile-project-root))
  ;; 	  (helm-rg "" nil (list (projectile-project-root)))))
  :bind (("C-c s" . helm-rg)
		 :map projectile-mode-map
		 ("C-c p s" . helm-projectile-rg)))

;;_________________________________________________Advanced version of i-search
;; https://github.com/ShingoFukuyama/helm-swoop
(use-package helm-swoop
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :config
  (setq helm-swoop-split-with-multiple-windows t)
  :bind
  ("M-i" . helm-swoop))

(use-package helm-make
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :delight
  :bind (("C-c p RET" . helm-make-projectile))
  :init
  (setq helm-make-comint t))

(use-package helm-projectile
  :after (helm projectile)
  :ensure helm
  :ensure projectile
  :ensure helm-rg
  :ensure t
  :defer t
  :delight
  :init
  ;; (add-hook 'c-mode-common-hook (lambda () (helm-projectile-on)))
  (add-hook 'projectile-mode-hook (lambda () (helm-projectile-on)))
  :config
  (define-key projectile-mode-map (kbd "C-c p s") 'helm-projectile-rg))

(use-package color-moccur
  :ensure t
  :defer t)

(use-package helm-c-moccur
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :bind ("C-c h o" . helm-occur))

;;____________________Describe keybindings for all open modes in current buffer
(use-package helm-descbinds
  :after helm
  :ensure helm
  :ensure t
  :defer t
  :bind ("C-c h C-h" . helm-descbinds))

;; some ugly functions
(load-file "~/.emacs-ugly.el")

;;______________________________________________________________________________

(provide '.emacs)
