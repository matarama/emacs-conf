
;;__________________________________Eclim for Java Development (eclipse server)
;; http://www.goldsborough.me/emacs,/java/2016/02/24/22-54-16-setting_up_emacs_for_java_development/
(use-package eclim
  :delight
  :ensure t
  :defer t
  :init
  (setq eclimd-autostart t)
  (add-hook 'java-mode-hook 'eclim-mode)
  (setq eclimd-wait-for-process nil)
  (custom-set-variables
   '(eclim-eclipse-dirs '("/opt/eclipse"))
   '(eclim-executable "/opt/eclipse/eclim")
   '(eclimd-executable "/opt/eclipse/eclimd")
   '(eclimd-default-workspace "~/workspace-eclipse"))
  (require 'eclimd)
  :bind (:map java-mode-map
              ("M-." . eclim-java-find-declaration)))

(use-package company-emacs-eclim
  :after eclim
  :ensure t
  :ensure eclim
  :defer t
  :delight
  :init
  (setq company-emacs-eclim-ignore-case t)
  (company-emacs-eclim-setup))

;;____________________________________________Gradle for building java projects
;; http://www.goldsborough.me/emacs,/java/2016/02/24/22-54-16-setting_up_emacs_for_java_development/
(use-package gradle-mode
  :delight
  :ensure t
  :defer t
  :init
  (add-hook 'java-mode-hook 'gradle-mode)
  ;; :bind
  ;; (:map gradle-mode-map
  ;;       ("C-c C-r") 'build-and-run)
  :config
  (defun build-and-run ()
    (interactive)
    (gradle-run "build run"))

  (define-key gradle-mode-map (kbd "C-c C-r") 'build-and-run)

  (setenv "PATH" (concat (getenv "PATH") ":/opt/gradle-4.7/bin/gradle"))
  (setq exec-path (append exec-path '("/opt/gradle-4.7/bin/gradle"))))
