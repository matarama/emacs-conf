;;__________________________________________________________________Set C style
;; Available C styles:
;; “gnu”: The default style for GNU projects
;; “k&r”: What Kernighan and Ritchie, the authors of C used in their book
;; “bsd”: What BSD developers use, aka “Allman style” after Eric Allman.
;; “whitesmith”: Popularized by the examples that came with Whitesmiths C,
;;               an early commercial C compiler.
;; “stroustrup”: What Stroustrup, the author of C++ used in his book
;; “ellemtel”: Popular C++ coding standards as defined by “Programming in C++,
;;             Rules and Recommendations,”
;;             Erik Nyquist and Mats Henricson, Ellemtel
;; “linux”: What the Linux developers use for kernel development
;; “python”: What Python developers use for extension modules
;; “java”: The default style for java-mode (see below)
;; “user”: When you want to define your own style
(setq c-default-style "ellemtel" )

;;______________________________________Syntax highlighting for modern cpp code
(use-package modern-cpp-font-lock
  :delight modern-c++-font-lock-mode
  :ensure t
  :defer t
  :init
  (add-hook 'c++-mode-hook #'modern-c++-font-lock-mode)
  ;; (modern-c++-font-lock-global-mode t)
  )

;;_________________________________________________________________________TAGS
;; (setq enable-rtags (file-exists-p "/opt/rtags/build/bin/rc"))
;; (setq enable-ggtags
;; 	  (and (not enable-rtags)
;; 		   (file-exists-p "/opt/global-6.6.3/install/bin/global")))

;;________________________________________gnu-global for source code navigation
;; http://tuhdo.github.io/c-ide.html
;; (use-package ggtags
;;   :if enable-ggtags
;;   :delight ggtags
;;   :ensure t
;;   :defer t
;;   :init
;;   ;(add-hook 'c++-mode-hook (lambda () (ggtags-mode 1)))
;;   ;(add-hook 'c-mode-hook (lambda () (ggtags-mode 1)))
;;   ;(add-hook 'c-mode-common-hook (lambda () (ggtags-mode 1)))
;;   :bind (:map ggtags-mode-map
;; 		 ("C-c g s" . ggtags-find-other-symbol)
;; 		 ("C-c g h" . ggtags-view-tag-history)
;; 		 ("C-c g r" . ggtags-find-reference)
;; 		 ("C-c g f" . ggtags-find-file)
;; 		 ("C-c g c" . ggtags-create-tags)
;; 		 ("C-c g u" . ggtags-update-tags)
;; 		 ("M-," . pop-tag-mark))
;;   :config
;;   ;; The value of environment variable “PATH” is used by emacs when you are
;;   ;; running a shell in emacs, similar to when you are using a shell in a
;;   ;; terminal.
;;   (setenv "PATH"
;; 		  (concat (getenv "PATH") ":/opt/global-6.6.3/install/bin/global"))

;;   ;; The exec-path is used by emacs itself to find programs it needs for its
;;   ;; features, such as spell checking, file compression, compiling, grep,
;;   ;; diff, etc.
;;   (setq exec-path
;; 		(append exec-path '("/opt/global-6.6.3/install/bin/global"))))

;; (use-package helm-gtags
;;   :if enable-ggtags
;;   :ensure helm
;;   :ensure t
;;   :defer t
;;   :delight
;;   :init
;;   ;; (add-hook 'dired-mode-hook 'helm-gtags-mode)
;;   ;; (add-hook 'eshell-mode-hook 'helm-gtags-mode)
;;   ;; (add-hook 'c-mode-hook 'helm-gtags-mode)
;;   ;; (add-hook 'c++-mode-hook 'helm-gtags-mode)
;;   ;; (add-hook 'asm-mode-hook 'helm-gtags-mode)
;;   :bind (:map helm-gtags-mode-map
;;               ;; ("M-;" . rtags-find-file)
;;   			  ;; ("C-," . rtags-find-references)
;;   			  ("M-." . gtags-find-tag)
;;   			  ("M-," . gtags-find-rtag)
;;   			  ("C-." . ggtags-find-symbol)
;;   			  ;; ("M-i" . idomenu)
;; 			  ("C-c g a" . helm-gtags-tags-in-this-function)
;; 			  ("C-j" . helm-gtags-select)
;; 			  ;; ("M-." . helm-gtags-dwim)
;; 			  ;; ("M-," . helm-gtags-pop-stack)
;; 			  ("C-c <" . helm-gtags-previous-history)
;; 			  ("C-c >" . helm-gtags-next-history))
;;   )

;;_____________________________________________rtags for source code navigation
;; (use-package rtags
;;   ;; :if enable-rtags
;;   :ensure t
;;   :defer t
;;   :delight
;;   ;; :init
;;   ;; (define-key c-mode-base-map (kbd "M-.") 'rtags-find-symbol-at-point)
;;   ;; (define-key c-mode-base-map (kbd "M-,") 'rtags-find-references-at-point)
;;   ;; (define-key c-mode-base-map (kbd "C-.") 'rtags-find-symbol)
;;   ;; (define-key c-mode-base-map (kbd "C-<") 'rtags-find-virtuals-at-point)
;;   ;; (define-key c-mode-base-map (kbd "M-i") 'rtags-imenu)
;;   :bind (:map c-mode-base-map
;;   			  ;; ("M-;" . rtags-find-file)
;;   			  ;; ("C-," . rtags-find-references)
;;   			  ("M-." . rtags-find-symbol-at-point)
;;   			  ("M-," . rtags-find-references-at-point)
;;   			  ("C-." . rtags-find-symbol)
;;   			  ("C-<" . rtags-find-virtuals-at-point)
;;   			  ;; ("M-i" . rtags-imenu)
;;               )
;;   )

;; (use-package helm-rtags
;;   :if enable-rtags
;;   :ensure helm
;;   :ensure rtags
;;   :ensure t
;;   :defer t
;;   :delight
;;   :init
;;   (setq rtags-display-result-backend 'helm)
;;   )

;; (use-package company-rtags
;;   :if enable-rtags
;;   :after (company rtags)
;;   :ensure company
;;   :ensure rtags
;;   :ensure t
;;   :defer t
;;   :init
;;   (add-to-list 'company-backends 'company-rtags)
;;   ;; (setq rtags-completions-enabled t)
;;   ;; (setq rtags-autostart-diagnostics t)
;;   )

;; (use-package flycheck-rtags
;;   :if enable-rtags
;;   :after (flycheck rtags)
;;   :ensure flycheck
;;   :ensure rtags
;;   :ensure t
;;   :defer t
;;   :delight
;;   :config
;;   (defun my-flycheck-rtags-setup ()
;; 	(flycheck-select-checker 'rtags)
;; 	(setq-local flycheck-highlighting-mode nil) ;; RTags creates more accurate overlays.
;; 	(setq-local flycheck-check-syntax-automatically nil))

;;   ;; c-mode-common-hook is also called by c++-mode
;;   (add-hook 'c-mode-common-hook #'my-flycheck-rtags-setup))

;;_____________________________________ccls for source code navigation and more

(use-package ccls
  :delight
  :ensure lsp-mode
  :ensure lsp-ui
  :ensure company-lsp
  :ensure t
  ;; :hook ((c-mode c++-mode objc-mode) .
  ;;        (lambda () (require 'ccls) (lsp)))
  :init
  (setq ccls-executable "/opt/ccls/install/bin/ccls")

  ;; Log file
  (setq ccls-args '("--log-file=/tmp/ccls.log"))

  (add-hook 'c++-mode-hook 'lsp-mode)
  (add-hook 'c-mode-hook 'lsp-mode))


;;______________________________________________CMake-ide for C/C++ development
;; (setq enable-cmake-ide enable-rtags)
;; (use-package cmake-ide
;;   :if enable-cmake-ide
;;   :delight
;;   :ensure t
;;   :ensure rtags
;;   :defer t
;;   :after rtags
;;   :init
;;   (require 'rtags)
;;   (global-set-key [f12] 'cmake-ide-compile)
;;   (put 'cmake-ide-build-dir 'safe-local-variable #'stringp)
;;   (setq cmake-ide-flags-c
;; 		'("/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/x86_64-pc-linux-gnu"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/backward"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include"
;; 		  "/usr/local/include"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include-fixed"
;; 		  "/usr/include"))
;;   (setq cmake-ide-flags-c++
;; 		'("/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/x86_64-pc-linux-gnu"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/backward"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include"
;; 		  "/usr/local/include"
;; 		  "/usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include-fixed"
;; 		  "/usr/include"))
;;   (setenv "PATH" (concat (getenv "PATH") ":/opt/rtags-2.18/build/bin"))
;;   (setq exec-path (append exec-path '("/opt/rtags-2.18/build/bin")))
;;   (cmake-ide-setup)
;;   (cmake-ide-maybe-start-rdm)
;; )

;;______________________________________________________________CODE-COMPLETION

;;_______________________________________YouCompleteMe for C++/C content assist
;; (setq enable-ycmd (file-exists-p "/opt/ycmd/ycmd"))
;; (use-package ycmd
;;   :if enable-ycmd
;;   :delight
;;   :ensure t
;;   :defer t
;;   :init
;;   (add-hook 'c++-mode-hook 'ycmd-mode)
;;   (set-variable 'ycmd-server-command '("python" "/opt/ycmd/ycmd/"))
;;   (set-variable 'ycmd-extra-conf-whitelist '("~/.ycm_extra_conf.py"))
;;   (set-variable 'ycmd-global-config "~/.ycm_extra_conf.py")

;;   (require 'ycmd-eldoc)
;;   (add-hook 'ycmd-mode-hook 'ycmd-eldoc-setup)
;;   )

;; (use-package company-ycmd
;;   :if enable-ycmd
;;   :ensure t
;;   :ensure company
;;   :ensure ycmd
;;   :after (company ycmd)
;;   :defer t
;;   :init
;;   (company-ycmd-setup)
;;   (add-to-list 'company-backends 'company-ycmd))

;; (use-package flycheck-ycmd
;;   :if enable-ycmd
;;   :after flycheck
;;   :ensure t
;;   :ensure flycheck
;;   :defer t
;;   :delight
;;   :init
;;   (flycheck-ycmd-setup)
;;   ;; (add-hook 'c++-mode-hook 'flycheck-mode)
;;   ;; (add-hook 'c-mode-hook 'flycheck-mode)
;;   (add-hook 'ycmd-file-parse-result-hook 'flycheck-ycmd--cache-parse-results)
;;   (add-to-list 'flycheck-checkers 'ycmd)
;;   (when (not (display-graphic-p))
;; 	(setq flycheck-indication-mode nil)))


;;__________________________________________________________________BUILD TOOLS

;;__________________________________________________________To edit cmake files
;; https://cmake.org/Wiki/CMake/Editors/Emacs
(use-package cmake-mode
  :ensure company
  :ensure t
  :defer t
  :delight cmake-mode
  :init
  (setenv "PATH" (concat (getenv "PATH") ":/opt/cmake-3.10.0/bin"))
  (setq exec-path (append exec-path '("/opt/cmake-3.10.0/bin/cmake")))
  ;; Add cmake listfile names to the mode list.
  (setq auto-mode-alist
		(append
		 '(("CMakeLists\\.txt\\'" . cmake-mode))
		 '(("\\.cmake\\'" . cmake-mode))
		 auto-mode-alist))

  (require 'company-cmake)
  (add-to-list 'company-backends 'company-cmake)
)

;; https://github.com/Lindydancer/cmake-font-lock
(use-package cmake-font-lock
  :after cmake-mode
  :ensure cmake-mode
  :ensure t
  :defer t
  :delight
  :init
  (autoload 'cmake-font-lock-activate "cmake-font-lock" nil t)
  (add-hook 'cmake-mode-hook 'cmake-font-lock-activate))


;;________________________________________________________________DOCUMENTATION

(when (require 'helm-dash nil 'noerror)
  (helm-dash-ensure-docset-installed "C")
  (helm-dash-ensure-docset-installed "C++")
  (helm-dash-ensure-docset-installed "Boost")
  (helm-dash-ensure-docset-installed "CMake")

  (add-hook 'c-mode-hook (lambda () (helm-dash-activate-docset "C")))
  (add-hook 'c++-mode-hook (lambda () (helm-dash-activate-docset "C")))
  (add-hook 'c++-mode-hook (lambda () (helm-dash-activate-docset "C++")))
  (add-hook 'c++-mode-hook (lambda () (helm-dash-activate-docset "Boost")))
  (add-hook 'cmake-mode-hook (lambda () (helm-dash-activate-docset "CMake"))))
